﻿using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Azure.WebJobs.Host.Bindings;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using Weario.Integration.Application.Configuration;
using Weario.Integration.Application.Extensions;

[assembly: FunctionsStartup(typeof(Weario.Functions.Startup))]
namespace Weario.Functions
{
    public class Startup : FunctionsStartup
    {
        private ILoggerFactory _loggerFactory;


        public override void Configure(IFunctionsHostBuilder builder)
        {
            builder.Services.AddOptions<LocalSettings>()
                .Configure<IConfiguration>((settings, configuration) =>
                {
                    configuration.Bind(settings);
                });
            builder.Services.AddOptions<GeneralSettings>()
                .Configure<IConfiguration>((settings, configuration) =>
                {
                    configuration.GetSection("GeneralSettings").Bind(settings);
                });
            builder.Services.AddOptions<Participants>()
                .Configure<IConfiguration>((settings, configuration) =>
                {
                    configuration.GetSection("Participants").Bind(settings);
                });

            builder.Services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.SetMinimumLevel(LogLevel.Debug);
            });


        }


        public class LocalSettings
        {
            public Supplier Supplier { get; set; }
        }

        public class MyOptions
        {
            public string MyCustomSetting { get; set; }
        }

        public class Supplier
        {
            public List<ProductFeedSettings> ProductFeeds { get; set; }
        }

        public class ProductFeedSettings
        {
            public string LanguageCode { get; set; }
            public string Url { get; set; }
            public AuthenticationSettings Authentication { get; set; }
            public int MaxProductsPerBatch { get; set; } = 100;
            public string AssortmentId { get; set; }
            public int LimitRequestSize { get; set; }
        }


        public class AuthenticationSettings
        {
            public string Schema { get; set; }
            public string Parameter { get; set; }
        }
    }

}
