﻿using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;

namespace Weario.Integration.Infrastructure.Clients.Distributor
{
    public partial class DistributorApiClient
    {
        private string _url;
        private AuthenticationHeaderValue _authentication;
        private Regex _pathRegex;

        public DistributorApiClient(System.Net.Http.HttpClient httpClient, string url, AuthenticationHeaderValue authentication) : this(httpClient)
        {
            _url = url.EndsWith("/")?  url : $"/{url}";
            _authentication = authentication;
            _pathRegex = new Regex(@"(?<url>.+)(?:\/order(?<guid>\/[^\/\?&]+)|\/product)$", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);
        }

        partial void PrepareRequest(System.Net.Http.HttpClient client, System.Net.Http.HttpRequestMessage request, StringBuilder urlBuilder)
        {
            urlBuilder.Insert(0, _url);
            request.Headers.Authorization = _authentication;

            var path = urlBuilder.ToString();
            path = _pathRegex.Replace(path, "$1$2");
            
            urlBuilder.Clear().Append(path);
        }
    }
}
