﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Weario.Integration.Infrastructure.Clients
{
    public interface IBaseHttpClient
    {
        HttpClient Client { get; }
    }

    public class BaseHttpClient : IBaseHttpClient
    {

        public HttpClient Client { get; private set; }

        public BaseHttpClient(HttpClient httpClient)
        {
            Client = httpClient;
        }
    }
}
