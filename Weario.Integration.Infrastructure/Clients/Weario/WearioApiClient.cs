﻿using Newtonsoft.Json;
using System;
using System.Net;

namespace Weario.Integration.Infrastructure.Clients.Weario
{
    public partial class WearioApiClient
    {
        partial void ProcessResponse(System.Net.Http.HttpClient client, System.Net.Http.HttpResponseMessage response)
        {

            if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                string responseData_ = null;
                ValidationErrorsResponse validationErrorsResponse = null;

                try
                {
                    responseData_ = response.Content == null ? null : response.Content.ReadAsStringAsync().ConfigureAwait(false).GetAwaiter().GetResult();
                    validationErrorsResponse = JsonConvert.DeserializeObject<ValidationErrorsResponse>(responseData_);
                }
                catch { }

                if (validationErrorsResponse != null)
                {
                    response.StatusCode = HttpStatusCode.OK;


                    var headers_ = System.Linq.Enumerable.ToDictionary(response.Headers, h_ => h_.Key, h_ => h_.Value);
                    if (response.Content != null && response.Content.Headers != null)
                    {
                        foreach (var item_ in response.Content.Headers)
                            headers_[item_.Key] = item_.Value;
                    }

                    throw new ApiException<ValidationErrorsResponse>(
                        "The HTTP status code of the response was not expected (" + (int)response.StatusCode + ").",
                        (int)response.StatusCode,
                        responseData_,
                        headers_,
                        validationErrorsResponse,
                        null
                    );
                }
            } else if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                string responseData_ = null;
                ValidationErrorsResponse validationErrorsResponse = null;

                try
                {
                    responseData_ = response.Content == null ? null : response.Content.ReadAsStringAsync().ConfigureAwait(false).GetAwaiter().GetResult();
                    validationErrorsResponse = JsonConvert.DeserializeObject<ValidationErrorsResponse>(responseData_);
                }
                catch { }

            }
        }
    }

    public partial class ValidationErrorsResponse
    {
        [Newtonsoft.Json.JsonProperty("Type", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public string Type { get; set; }

        [Newtonsoft.Json.JsonProperty("Message", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public string Message { get; set; }

        [Newtonsoft.Json.JsonProperty("ValidationErrors", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public System.Collections.Generic.ICollection<ValidationError> ValidationErrors { get; set; }
    }

    public partial class ValidationError
    {
        [Newtonsoft.Json.JsonProperty("Message", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public string Message { get; set; }

        //[Newtonsoft.Json.JsonProperty("ProductID", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        //public string ProductID { get; set; }

        [Newtonsoft.Json.JsonProperty("FieldName", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public string FieldName { get; set; }

        [Newtonsoft.Json.JsonProperty("ValidationContext", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public string ValidationContext { get; set; }
    }

}
