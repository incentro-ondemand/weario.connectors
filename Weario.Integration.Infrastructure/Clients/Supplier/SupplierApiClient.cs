﻿using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Text;

namespace Weario.Integration.Infrastructure.Clients.Supplier
{
    public partial class SupplierApiClient
    {
        private string _url;
        private AuthenticationHeaderValue _authentication;

        public SupplierApiClient(System.Net.Http.HttpClient httpClient, string url, AuthenticationHeaderValue authentication) : this(httpClient)
        {
            this._url = url.EndsWith("/")?  url : $"/{url}";
            this._authentication = authentication;
        }

        partial void PrepareRequest(System.Net.Http.HttpClient client, System.Net.Http.HttpRequestMessage request, StringBuilder urlBuilder)
        {
            urlBuilder.Insert(0, _url);
            request.Headers.Authorization = _authentication;
        }
    }
}
