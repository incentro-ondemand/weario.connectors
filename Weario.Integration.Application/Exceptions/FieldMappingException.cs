﻿using System;

namespace Weario.Integration.Application.Exceptions
{
    public class FieldMappingException : Exception
    {
        public string Identifier { get; private set; }
        public string Field { get; private set; }
        public string Reason { get; private set; }

        public FieldMappingException(string field) : base($"{field} - Empty")
        {
            this.Identifier = null;
            this.Field = field;
            this.Reason = "Empty";
        }
        public FieldMappingException(string field, string reason) : base($"{field} - {reason}")
        {
            this.Identifier = null;
            this.Field = field;
            this.Reason = reason;
        }
        public FieldMappingException(string identifier, string field, string reason) : base($"{identifier}:{field} - {reason}")
        {
            this.Identifier = identifier;
            this.Field = field;
            this.Reason = reason;
        }
    }
}
