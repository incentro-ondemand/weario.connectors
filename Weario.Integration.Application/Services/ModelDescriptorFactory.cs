﻿using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Reflection;
using Weario.Integration.Application.Configuration;
using Weario.Integration.Application.ModelDescriptors;

namespace Weario.Integration.Application.Services
{
    /// <summary>
    /// Interface for a factory that creates a ModelDescriptor.
    /// </summary>
    /// <remarks>
    /// We have these ModelDescriptor classes because our models are generated based on Swagger definitions which don't contain nice generic structures.
    /// </remarks>
    public interface IModelDescriptorFactory
    {
        ModelDescriptorBase<T> Create<T>() where T : IModelDescriptor;
    }

    /// <summary>
    /// IModelDescriptorFactory implementation creates a ModelDescriptor.
    /// </summary>
    public class ModelDescriptorFactory : IModelDescriptorFactory
    {
        private readonly IOptions<GeneralSettings> _appSettings;

        public ModelDescriptorFactory(IOptions<GeneralSettings> appSettings)
        {
            _appSettings = appSettings;
        }

        /// <summary>
        /// Creates a ModelDescriptorBase<> based on the model type.
        /// </summary>
        public ModelDescriptorBase<T> Create<T>() where T : IModelDescriptor
        {
            var parentType = typeof(ModelDescriptorBase<>).MakeGenericType(typeof(T));

            var type = Assembly
                .GetExecutingAssembly()
                .GetTypes()
                .FirstOrDefault(t => t.IsSubclassOf(parentType))
                ?? throw new Exception($"Could not find ModelDescriptor<{typeof(T).Name}> implementation.");

            return Activator.CreateInstance(type,  _appSettings) as ModelDescriptorBase<T>;
        }
    }
}
