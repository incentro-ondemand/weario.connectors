﻿using Dasync.Collections;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Logging;
using Weario.Integration.Application.Configuration;
using Weario.Integration.Application.Extensions;
using Microsoft.Extensions.Options;
using System.Linq;

namespace Weario.Integration.Application.Services
{

    /// <summary>
    /// Describes a MessageSender for a servicebus that can send multiple messages in parallel. 
    /// </summary>
    public interface IMessageSender
    {
        Task SendMessage(string topic, string label, string message, Dictionary<string, object> properties = null);
        Task SendMessages(string topic, IList<Message> messages);
        Task ScheduleMessage(string topic, Message message, DateTimeOffset scheduleEnqueueTimeUtc);
        Message CreateMessage(string label, string body, Dictionary<string, object> properties = null);
    }

    /// <summary>
    /// IParallelMessageSender implementation that can send multiple servicebus messages in parallel.
    /// </summary>
    public class ParallelMessageSender : IMessageSender
    {
        private readonly ILogger _log;
        private GeneralSettings _appSettings;

        public ParallelMessageSender(
            ILogger<ParallelMessageSender> logger,
            IOptions<GeneralSettings> appSettings)
        {
            _log = logger;
            _appSettings = appSettings.Value ?? throw new ArgumentNullException(nameof(appSettings));
        }

        public Task SendMessage(string topic, string label, string message, Dictionary<string, object> properties = null)
            => ProcessMessages(
                topic,
                new[] { this.CreateMessage(label, message, properties) },
                (client, msg) => client.SendAsync(msg)
            );

        public Task SendMessages(string topic, IList<Message> messages)
            => ProcessMessages(
                topic, 
                messages, 
                (client, msg) => client.SendAsync(msg)
            );

        public Task ScheduleMessage(string topic, Message message, DateTimeOffset scheduleEnqueueTimeUtc)
            => ProcessMessages(
                topic, 
                new[] { message }, 
                (client, msg) => client.ScheduleMessageAsync(msg, scheduleEnqueueTimeUtc)
            );
        
        private async Task ProcessMessages(string topic, IList<Message> messages, Func<TopicClient, Message, Task> processAction)
        {
            if (messages == null || messages.Count == 0)
            {
                return;
            }

            TopicClient client = null;

            //send the message batch
            try
            {
                _log.LogInformation("Start processing message to topic.");

                //create the topic client
                client = new TopicClient(_appSettings.ServiceBus.ConnectionString, topic);

                //process messages in parallel
                await messages.ParallelForEachAsync(
                    async (message) => await processAction(client, message),
                    _appSettings.ServiceBus.MaxParallelism
                    );
            }
            catch (Exception ex)
            {
                _log.LogError(ex);

                throw;
            }
            finally
            {
                _log.LogInformation("Finished processing message to topic.");

                await client?.CloseAsync();
            }
        }

        public Message CreateMessage(string label, string body, Dictionary<string, object> properties = null)
        {
            //build the message
            Message message = new Message()
            {
                Label = label,
                Body = Encoding.UTF8.GetBytes(body)
            };

            //add custom properties
            if (properties?.Count > 0)
            {
                foreach (string key in properties.Keys)
                {
                    message.UserProperties[key] = properties[key];
                }
            }
            return message;
        }
    }
}
