﻿using Dasync.Collections;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.ServiceBus.Management;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Weario.Integration.Application.Configuration;
using Weario.Integration.Application.Extensions;
using Weario.Integration.Application.Helpers;
using Weario.Integration.Application.ModelDescriptors;

namespace Weario.Integration.Application.Services
{
    /// <summary>
    /// Interface for managing the servicebus
    /// </summary>
    public interface IServiceBusManager
    {
        Task EnsureParticipantsSubscriptions<T>() where T : IModelDescriptor;

        Task EnsureParticipantSubscription<T>(string participantID) where T: IModelDescriptor;

        string GetParticipantSubscriptionName(IParticipant participant);
    }

    /// <summary>
    /// IServiceBusManager implemenation for managing the servicebus
    /// </summary>
    public class ServiceBusManager : IServiceBusManager
    {
        private readonly ILogger _log;
        private readonly Participants _participants;
        private readonly IModelDescriptorFactory _descriptorFactory;
        private readonly GeneralSettings _appSettings;

        /// <summary>
        /// Constructor
        /// </summary>
        public ServiceBusManager(
            ILogger<ServiceBusManager> logger,
            IOptions<Participants> participants, 
            IModelDescriptorFactory descriptorFactory, 
            IOptions<GeneralSettings> appSettings)
        {
            _log = logger;
            _participants = participants.Value ?? throw new ArgumentNullException(nameof(participants));
            _descriptorFactory = descriptorFactory ?? throw new ArgumentNullException(nameof(descriptorFactory));
            _appSettings = appSettings.Value ?? throw new ArgumentNullException(nameof(appSettings));
        }

        /// <summary>
        /// Makes sure that the given topic has a subscription for the given participant
        /// </summary>
        public async Task EnsureParticipantSubscription<T>(string participantID) where T : IModelDescriptor
        {
            await this.PerformServiceBusWorkForParticipants<T>(async (client, participants) => {

                using (var scope = _log.BeginScope("{serviceName} securing participant subscription", nameof(ServiceBusManager)))
                {
                    try
                    {
                        var participant = participants
                            .FirstOrDefault(p => String.Equals(p.ID, participantID, StringComparison.InvariantCultureIgnoreCase))
                            ?? throw new Exception($"No participant was found for id '{participantID}'");

                        await this.CreateSubscriptions<T>(client, participant);

                    }
                    catch (Exception ex)
                    {
                        _log.LogError(ex);

                        throw new Exception("Could not secure participant subscription", ex);
                    }
                }
            });
        }

        /// <summary>
        /// Makes sure that the given topic has a subscription for each participant in the config
        /// </summary>
        public async Task EnsureParticipantsSubscriptions<T>() where T : IModelDescriptor
        {
            await this.PerformServiceBusWorkForParticipants<T>(async (client, participants) => {

                using (var scope = _log.BeginScope("{serviceName} securing participant subscriptions", nameof(ServiceBusManager)))
                {
                    try
                    {
                        //get all existing subscriptions
                        var subscriptions = await this.GetSubscriptions<T>(client);

                        //check which ones we need to create
                        var missing = participants
                            .Where(p => !subscriptions.Any(s => String.Equals(s.SubscriptionName, this.GetParticipantSubscriptionName(p), StringComparison.InvariantCultureIgnoreCase)))
                            .ToArray();

                        _log.LogInformation("Found {participants} missing participants", missing.Length);

                        //create the subscriptions
                        await this.CreateSubscriptions<T>(client, missing);
                    }
                    catch (Exception ex)
                    {
                        _log.LogError(ex);

                        throw new Exception("Could not secure participant subscriptions", ex);
                    }
                }
            });
        }


        private async Task PerformServiceBusWorkForParticipants<T>(Func<ManagementClient, IEnumerable<IParticipant>, Task> work) where T : IModelDescriptor
        {
            //create the descriptor for the given type. This way we can process different types in a singular manner.
            ModelDescriptorBase<T> descriptor = _descriptorFactory.Create<T>();

            //get all participants from the config
            var participants = _participants.Members(descriptor.Group);

            ManagementClient client = null;

            try
            {
                //create the management client
                client = new ManagementClient(_appSettings.ServiceBus.ConnectionString);

                await work(client, participants);
            }
            catch (Exception ex)
            {
                _log.LogError(ex);

                throw;
            }
            finally
            {
                client?.CloseAsync();
            }
        }

        /// <summary>
        /// Creates the subscription for the given participant
        /// </summary>
        private Task CreateSubscriptions<T>(ManagementClient client, params IParticipant[] participants) where T : IModelDescriptor
        {
            //create the descriptor for the given type. This way we can process different types in a singular manner.
            ModelDescriptorBase<T> descriptor = _descriptorFactory.Create<T>();

            return participants.ParallelForEachAsync(async (participant) =>
            {
                //get the subscription name
                var subscriptionName = this.GetParticipantSubscriptionName(participant);

                //create the subscription description
                var subscriptionDescription = new SubscriptionDescription(descriptor.Topic, subscriptionName)
                {
                    UserMetadata = participant.Name,
                    MaxDeliveryCount = _appSettings.ServiceBus.DefaultMaxDeliveryCount
                };

                //add a rule to the subscription so messages get redirected here
                var subscriptionRule = new RuleDescription("$Default", new SqlFilter($"{TopicSubscriptions.ParticipantIDs[participant.Group]} = '{participant.ID}'"));

                //check if the subscription doesn't exist 
                if (!await client.SubscriptionExistsAsync(subscriptionDescription.TopicPath, subscriptionDescription.SubscriptionName))
                {
                    //create the subscription
                    subscriptionDescription = await client.CreateSubscriptionAsync(subscriptionDescription, subscriptionRule);


                    _log.LogInformation("Created subscription {participant}", subscriptionDescription.SubscriptionName);
                }
            }, _appSettings.ServiceBus.MaxParallelism);
        }

        /// <summary>
        /// Gets all subscriptions
        /// </summary>
        private async Task<IEnumerable<SubscriptionDescription>> GetSubscriptions<T>(ManagementClient manager) where T : IModelDescriptor
        {
            //create the descriptor for the given type. This way we can process different types in a singular manner.
            ModelDescriptorBase<T> descriptor = _descriptorFactory.Create<T>();

            bool loop = true;

            int count = 0;
            int max = 100;

            List<SubscriptionDescription> all = new List<SubscriptionDescription>();

            //get all subscriptions per page (the api has a max of 100 items per page)
            while (loop)
            {
                var page = await manager.GetSubscriptionsAsync(descriptor.Topic, max, count);

                foreach (var subscription in page)
                {
                    all.Add(subscription);
                }

                count += page.Count;

                loop = page.Count == max;
            }

            return all;
        }

        /// <summary>
        /// Determines the name of the participant's subscription 
        /// </summary>
        public string GetParticipantSubscriptionName(IParticipant participant)
        {
            string s = participant.Code?.ToUpper();
            if (string.IsNullOrWhiteSpace(s)) s = CultureInfo.InvariantCulture.TextInfo.ToTitleCase(participant.Name.ToLower());
            s = (Regex.Replace(s, "[^A-Z0-9]", "") + "__________").Substring(0, 10);
            return $"{s}_{participant.ID}";
        }
    }
}
