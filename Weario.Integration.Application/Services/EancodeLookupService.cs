﻿using Microsoft.Azure.Cosmos.Table;
using Microsoft.Azure.WebJobs.Logging;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Weario.Integration.Application.Configuration;
using Weario.Integration.Application.DomainModels;

namespace Weario.Integration.Application.Services
{
    public interface IEancodeLookupService
    {
        Task<IEnumerable<EancodeLookupEntity>> FindSupplierProducts(long eanCode);

        public Task ProcessDistributorProducts(PostDistributorProducts postDistributorProducts);

        Task<EancodeLookupEntity> UpsertSupplierProduct(EancodeLookupEntity eancodeLookup);

        Task ProcessSupplierProducts(ICollection<SupplierProduct> products, Supplier supplier);
    }

    public class EancodeLookupService : IEancodeLookupService
    {
        private readonly ILogger _log;
        private readonly Participants _participants;
        private readonly IModelDescriptorFactory _descriptorFactory;
        private readonly GeneralSettings _appSettings;
        private CloudTable _table;

        /// <summary>
        /// Constructor
        /// </summary>
        public EancodeLookupService(
            ILogger<EancodeLookupService> logger,
            IOptions<Participants> participants,
            IModelDescriptorFactory descriptorFactory,
            IOptions<GeneralSettings> appSettings)
        {
            _log = logger;
            _participants = participants.Value ?? throw new ArgumentNullException(nameof(participants));
            _descriptorFactory = descriptorFactory ?? throw new ArgumentNullException(nameof(descriptorFactory));
            _appSettings = appSettings.Value ?? throw new ArgumentNullException(nameof(appSettings));
        }

        public async Task<CloudTable> Table()
        {
            if (_table == null)
            {
                string storageConnectionString = _appSettings.Storage.ConnectionString;
                string tableName = _appSettings.Storage.TableEancodeLookup;

                // Retrieve storage account information from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storageConnectionString);

                // Create a table client for interacting with the table service
                CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

                // Create a table client for interacting with the table service 
                _table = tableClient.GetTableReference(tableName);
                if (await _table.CreateIfNotExistsAsync())
                {
                    _log.LogInformation("Created Table named: {0}", tableName);
                }
                else
                {
                    _log.LogInformation("Table {0} already exists", tableName);
                }
            }
            return _table;
        }

        public async Task<IEnumerable<EancodeLookupEntity>> FindSupplierProducts(long eanCode)
        {
            // Connect to storage table
            var table = await this.Table();

            //var condition = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, eanCode.ToString());
            var condition = TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, eanCode.ToString());
            var query = new TableQuery<EancodeLookupEntity>().Where(condition);
            var lst = table.ExecuteQuery(query);

            return lst.ToList();
        }

        public async Task ProcessDistributorProducts(PostDistributorProducts postDistributorProducts)
        {
            foreach (var product in postDistributorProducts.Products)
            {
                if (product.UsesEANCodeIdentification)
                {
                    foreach (var variant in product.ProductVariants)
                    {
                        var ean = await FindSupplierProducts(variant.DistributorProductVariantEANCode.Value);
                        if (ean.Count() > 0)
                        {
                            product.SupplierID = ean.First().SupplierID;
                            product.SupplierProductID = ean.First().SupplierProductID;
                            goto nextProduct;
                        }
                    }
                }
                else
                {
                    foreach (var color in product.Colors)
                    {
                        foreach (var size in color.Sizes.Where(s => (s.DistributorProductVariantEANCode ?? 0) != 0))
                        {
                            var ean = await FindSupplierProducts(size.DistributorProductVariantEANCode.Value);
                            if (ean.Count() > 0)
                            {
                                product.SupplierID = ean.First().SupplierID;
                                product.SupplierProductID = ean.First().SupplierProductID;
                                goto nextProduct;
                            }
                        }
                    }
                }
            nextProduct:;
            }
        }


        public async Task<EancodeLookupEntity> UpsertSupplierProduct(EancodeLookupEntity eancodeLookup)
        {
            if (eancodeLookup == null)
            {
                throw new ArgumentNullException("entity");
            }
            try
            {
                // Create the InsertOrReplace table operation
                TableOperation insertOrMergeOperation = TableOperation.InsertOrMerge(eancodeLookup);

                // Connect to storage table
                var table = await this.Table();

                // Execute the operation.
                TableResult result = await _table.ExecuteAsync(insertOrMergeOperation);
                EancodeLookupEntity insertedEntity = result.Result as EancodeLookupEntity;

                return insertedEntity;
            }
            catch (StorageException ex)
            {
                _log.LogError(ex, ex.Message);
                throw;
            }
        }

        public async Task ProcessSupplierProducts(ICollection<SupplierProduct> products, Supplier supplier)
        {
            TableBatchOperation batchOperation = new TableBatchOperation();
            var entities = new List<string>();
            foreach (var p in products.Where(pr => pr.Colors != null))
            {
                foreach (var c in p.Colors.Where(co => co.Sizes != null))
                {
                    foreach (var size in c.Sizes.Where(s => (s.SupplierProductVariantEANCode ?? 0) != 0))
                    {
                        var eancodeLookup =
                            new EancodeLookupEntity(
                                size.SupplierProductVariantEANCode.ToString(),
                                supplier.ID)
                            {
                                SupplierProductID = p.SupplierProductID,
                                SupplierProductVariantID = size.SupplierProductVariantID
                            };
                        if (entities.Contains(eancodeLookup.RowKey) == false)
                        {
                            entities.Add(eancodeLookup.RowKey);
                            batchOperation.Add(TableOperation.InsertOrMerge(eancodeLookup));
                        }
                        else
                        {
                           _log.LogWarning("Ean code {eancode} not found.", eancodeLookup.RowKey);
                        }

                        if (entities.Count == 100)
                        {
                            await (await this.Table()).ExecuteBatchAsync(batchOperation);
                            batchOperation = new TableBatchOperation();
                            entities = new List<string>();
                        }
                    }
                }
            }
            if (entities.Count > 0)
            {
                await (await this.Table()).ExecuteBatchAsync(batchOperation);
            }
        }
    }
}
