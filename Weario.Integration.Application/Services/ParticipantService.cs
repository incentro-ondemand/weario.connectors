﻿using Microsoft.Azure.WebJobs.Logging;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using Weario.Integration.Application.Configuration;

namespace Weario.Integration.Application.Services
{
    public interface IParticipantService
    {
        bool ValidateParticipant(string participantId, ParticipantGroup group);
    }

    public class ParticipantService : IParticipantService
    {
        private readonly ILogger _log;
        private readonly Participants _participants;

        public ParticipantService(
            ILogger<ParticipantService> logger,
            IOptions<Participants> participants
            )
        {
            _log = logger;
            _participants = participants.Value ?? throw new ArgumentNullException(nameof(participants));
        }


        public bool ValidateParticipant(string participantId, ParticipantGroup group)
        {
            switch (group)
            {
                case ParticipantGroup.Distributors:
                    return _participants.Distributors != null && _participants.Distributors.Any(p => string.Equals(p.ID, participantId, StringComparison.InvariantCultureIgnoreCase));
                case ParticipantGroup.Suppliers:
                    return _participants.Suppliers != null && _participants.Suppliers.Any(p=> string.Equals(p.ID, participantId, StringComparison.InvariantCultureIgnoreCase));
                default: 
                    return false;
            }
        }
    }
}
