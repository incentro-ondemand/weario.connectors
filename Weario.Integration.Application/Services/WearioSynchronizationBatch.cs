﻿using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.ServiceBus.Core;
using Microsoft.Azure.ServiceBus.Management;
using System;
using System.Collections.Generic;


namespace Weario.Integration.Application.Services
{
    /// <summary>
    /// Interface that contains the neccessary information/tools to process a single batch
    /// </summary>
    public interface IWearioSynchronizationBatch
    {
        MessageReceiver Receiver { get; }

        SubscriptionDescription Subscription { get; }

        IList<Message> Results { get; set; }

        Exception Exception { get; set; }
    }

    /// <summary>
    /// Class that holds batch related information during synchronisation
    /// </summary>
    public class WearioSynchronizationBatch : IWearioSynchronizationBatch
    {
        public WearioSynchronizationBatch(MessageReceiver receiver, SubscriptionDescription subscription)
        {
            this.Receiver = receiver;
            this.Subscription = subscription;
        }

        public MessageReceiver Receiver { get; }

        public SubscriptionDescription Subscription { get; }

        public IList<Message> Results { get; set; }

        public Exception Exception { get; set; }
    }
}
