﻿using Dasync.Collections;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.ServiceBus.Core;
using Microsoft.Azure.ServiceBus.Management;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Weario.Integration.Application.Configuration;
using Weario.Integration.Application.Extensions;
using Weario.Integration.Application.ModelDescriptors;
using Weario.Integration.Infrastructure.Clients;
using Weario.Integration.Infrastructure.Clients.Distributor;
using Weario.Integration.Infrastructure.Clients.Supplier;

namespace Weario.Integration.Application.Services
{
    public interface IDeadLetterService
    {
        Task ProcessDeadletterMessages<T>(CancellationToken cancellationToken) where T : IModelDescriptor;
    }

    public class DeadLetterService : IDeadLetterService
    {

        private readonly ILogger _logger;
        private readonly IServiceBusManager _serviceBusManager;
        private readonly GeneralSettings _appSettings;
        private readonly Participants _participants;
        private readonly IWearioSynchronizer _wearioSynchronizer;
        private readonly IModelDescriptorFactory _descriptorFactory;
        private readonly IBaseHttpClient _baseHttpClient;

        public DeadLetterService(
            ILogger<OrderManager> logger,
            IServiceBusManager serviceBusManager,
            IOptions<GeneralSettings> appSettings,
            IOptions<Participants> participants,
            IWearioSynchronizer wearioSynchronizer,
            IModelDescriptorFactory descriptorFactory,
            IBaseHttpClient baseHttpClient)
        {
            _logger = logger;
            _serviceBusManager = serviceBusManager ?? throw new ArgumentNullException(nameof(serviceBusManager));
            _appSettings = appSettings.Value ?? throw new ArgumentNullException(nameof(appSettings));
            _participants = participants.Value ?? throw new ArgumentNullException(nameof(participants));
            _wearioSynchronizer = wearioSynchronizer;
            _descriptorFactory = descriptorFactory;
            _baseHttpClient = baseHttpClient;
        }

        public async Task ProcessDeadletterMessages<T>(CancellationToken cancellationToken) where T : IModelDescriptor
        {
            var descriptor = _descriptorFactory.Create<T>();

            _logger.LogInformation($"Send deadletter {descriptor.Topic} to weario.");

            //make sure we have a subscription for each participant
            await _serviceBusManager.EnsureParticipantsSubscriptions<T>();

            List<WearioSynchronizationBatch> batches = null;
            try
            {
                int maxLoopCount = 1;
                do
                {
                    // Setup an collection on batches which contain a servicebus messagereceiver per batch. 
                    // Each beatch subscribes to its own subscription.
                    batches = await GetBatches(descriptor);

                    // Fill every batch with the deadletter messages from it's subscription.
                    // Each batch recieves a maximum of 'descriptor.SyncBatchSize' messages.
                    await GetBatchMessages(descriptor, batches);

                    // Process the deadletter messages from the batches.
                    // After processing the deadleter messages are completed.
                    await ProcessBatchedMessages(descriptor, batches);

                } while (--maxLoopCount > 0 && batches.Sum(b => b.Results.Count) > 0);
            }
            catch (Exception ex)
            {
                this.RaiseBatchExceptions(batches, ex);
            }
            finally
            {
                if (batches != null)
                {
                    Task.WaitAll(batches.Select(b => b.Receiver.CloseAsync()).ToArray());
                }
            }
            RaiseBatchExceptions(batches);
        }

        private async Task<List<WearioSynchronizationBatch>> GetBatches<T>(ModelDescriptorBase<T> descriptor) where T : IModelDescriptor
        {
            //create the manager
            ManagementClient manager = new ManagementClient(_appSettings.ServiceBus.ConnectionString);

            //prepare topic for message retrieval (create subscriptions, messagerecievers)
            try
            {
                _logger.LogInformation("Start preparing topic '{topic}' for deadletter message retrieval.", descriptor.Topic);

                //get the subscriptions
                var subscriptions = await manager.GetSubscriptionsAsync(descriptor.Topic);

                //prepare the batch retrieval
                return subscriptions
                    .Select(subscription => new WearioSynchronizationBatch(
                        new MessageReceiver(
                            _appSettings.ServiceBus.ConnectionString,
                            EntityNameHelper.FormatDeadLetterPath(
                                EntityNameHelper.FormatSubscriptionPath(subscription.TopicPath, subscription.SubscriptionName)
                            )
                        ),
                        subscription
                    ))
                    .ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                throw new Exception("Could not retrieve topic subscriptions.", ex);
            }
            finally
            {
                await manager.CloseAsync();
                _logger.LogInformation("Finished preparing topic for message retrieval.");
            }
        }

        private async Task GetBatchMessages<T>(ModelDescriptorBase<T> descriptor, List<WearioSynchronizationBatch> batches) where T : IModelDescriptor
        {
            //retrieve messages from the subscripions
            try
            {
                _logger.LogInformation("Start retrieving message batches from the topic '{topic}'. Batchcount: {Batchcount}", descriptor.Topic, batches.Count);

                //retrieve results in parallel
                await batches.ParallelForEachAsync(async (batch) =>
                {
                    try
                    {
                        batch.Results = await batch.Receiver.ReceiveAsync(descriptor.SyncBatchSize, _appSettings.SyncTimeout) ?? new List<Message>();
                    }
                    catch (Exception ex)
                    {
                        batch.Exception = ex;
                    }
                }, _appSettings.ServiceBus.MaxParallelism);

                //determine results
                int successCount = batches.Where(b => b.Exception == null && b.Results.Count > 0).Count();
                int emptyCount = batches.Where(b => b.Exception == null && b.Results.Count == 0).Count();
                int errorCount = batches.Where(b => b.Exception != null).Count();
                int messageCount = batches.Where(b => b.Exception == null).Sum(b => b.Results.Count());

                _logger.LogInformation("Found {successBatchCount} successful batches, {emptyBatchCount} empty batches, {errorBatchCount} failed batches, {messageCount} message(s) in total.",
                    successCount,
                    emptyCount,
                    errorCount,
                    messageCount
                    );
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);

                throw new Exception("Could not retrieve message(s).", ex);
            }
            finally
            {
                _logger.LogInformation("Finished retrieving message batches from the topic.");
            }
        }

        public async Task ProcessBatchedMessages<T>(ModelDescriptorBase<T> descriptor, List<WearioSynchronizationBatch> batches) where T : IModelDescriptor
        {
            //process results in parallel
            await batches
                .Where(b => b.Exception == null && b.Results.Count > 0)
                .ToList()
                .ParallelForEachAsync(async (batch) =>
                {
                    var participantID = batch.Results.Select(m => m.UserProperties[descriptor.ParticipantKeyField]).Cast<string>().FirstOrDefault();
                    var participant = _participants.Find(participantID);

                    using (_logger.BeginScope("{" + descriptor.ParticipantKeyField + "}", participantID))
                    {
                        try
                        {
                            _logger.LogInformation("Starting sending batch containing {messageCount} message(s) to weario.", batch.Results.Count, batch.Subscription.SubscriptionName);

                            // the given list of messages should all be of the same participant
                            if (batch.Results.GroupBy(m => m.UserProperties[descriptor.ParticipantKeyField]).Count() > 1)
                            {
                                throw new Exception("Can't send message to Weario because the batch contains messages of different participants.");
                            }

                            if (Type.Equals(descriptor.ModelType, typeof(DomainModels.DistributorOrders)))
                            {
                                foreach (var message in batch.Results)
                                {
                                    // Schedule a new feedback order using the failed distibutor order
                                    await _wearioSynchronizer.ProcessRequest(CreateFeedbackOrders(message));
                                    // Complete the distibutor order from sericebus
                                    await batch.Receiver.CompleteAsync(message.SystemProperties.LockToken);
                                }
                            }
                            else if (Type.Equals(descriptor.ModelType, typeof(DomainModels.PostDistributorProducts))
                                    && (participant?.ProductApi?.IsValid ?? false))
                            {
                                // Stel het array van failed producten samen wat terug gestuurd gaat worden naar de distributeur.
                                var failedProducts = batch.Results
                                    .Where(message =>
                                        message.UserProperties.Keys.Contains("ProductID") &&
                                        message.UserProperties.Keys.Contains("ValidationErrors")
                                    )
                                    .Select(message => 
                                        new Integration.Infrastructure.Clients.Distributor.FailedProduct()
                                        {
                                            ProductId = $"{message.UserProperties["ProductID"]}",
                                            Message = $"{message.UserProperties["ValidationErrors"]}"
                                        }
                                    );

                                if (failedProducts.Count() > 0)
                                {
                                    // Maak de client die we gaan gebruiken om de product feedback naar toe te versturen.
                                    ApiConsumer api = participant?.ProductApi;
                                    DistributorApiClient client = new DistributorApiClient(
                                        _baseHttpClient.Client,
                                        api.Url,
                                        new System.Net.Http.Headers.AuthenticationHeaderValue(api.Authentication.Schema, api.Authentication.Parameter)
                                    );

                                    // Verstuur de feedback.
                                    await client.ProductAsync(failedProducts);
                                }

                                // Ruim de deadletter messages op die we afgehandeld hebben.
                                Task.WaitAll(batch.Results.Select(message => batch.Receiver.CompleteAsync(message.SystemProperties.LockToken)).ToArray());
                            }
                            else if (Type.Equals(descriptor.ModelType, typeof(DomainModels.PostSupplierProducts))
                                    && (participant?.ProductApi?.IsValid ?? false))
                            {
                                // Stel het array van failed producten samen wat terug gestuurd gaat worden naar de supplier.
                                var failedProducts = batch.Results
                                    .Where(message =>
                                        message.UserProperties.Keys.Contains("ProductID") && 
                                        message.UserProperties.Keys.Contains("ValidationErrors")
                                    )
                                    .Select(message =>
                                        new Integration.Infrastructure.Clients.Supplier.FailedProduct()
                                        {
                                            ProductId = $"{message.UserProperties["ProductID"]}",
                                            Message = $"{message.UserProperties["ValidationErrors"]}"
                                        }
                                    );

                                if (failedProducts.Count() > 0)
                                {
                                    // Maak de client die we gaan gebruiken om de product feedback naar toe te versturen.
                                    ApiConsumer api = participant?.ProductApi;
                                    SupplierApiClient client = new SupplierApiClient(
                                        _baseHttpClient.Client,
                                        api.Url,
                                        new System.Net.Http.Headers.AuthenticationHeaderValue(api.Authentication.Schema, api.Authentication.Parameter)
                                    );

                                    // Verstuur de feedback.
                                    await client.ProductAsync(failedProducts);
                                }

                                // Ruim de deadletter messages op die we afgehandeld hebben.
                                Task.WaitAll(batch.Results.Select(message => batch.Receiver.CompleteAsync(message.SystemProperties.LockToken)).ToArray());
                            }
                            else
                            {
                                // Ruim de deadletter messages op waar we niks mee moeten doen.
                                Task.WaitAll(batch.Results.Select(message => batch.Receiver.CompleteAsync(message.SystemProperties.LockToken)).ToArray());
                            }
                        }
                        catch (Exception ex)
                        {
                            //Exception will be thrown after all batches are done.
                            batch.Exception = ex;

                            //cancel messages 
                            foreach (var message in batch.Results)
                            {
                                await batch.Receiver.AbandonAsync(message.SystemProperties.LockToken, message.UserProperties);
                            }
                        }
                        finally
                        {
                            _logger.LogInformation("Finished sending batch to weario.");
                        }
                    }
                }, _appSettings.WearioApi.MaxParallelism);
        }

        public DomainModels.WearioOrders CreateFeedbackOrders(Message message)
        {
            string distributorID = message.UserProperties["DistributorID"] as string;
            string jobIndentifier = message.UserProperties["JobIndentifier"] as string;
            string orderID = message.UserProperties["OrderID"] as string;

            using (_logger.BeginScope("{DistributorID}{JobIndentifier}", distributorID, jobIndentifier))
            {
                var distibutionOrders = JsonConvert.DeserializeObject<DomainModels.DistributorOrders>(Encoding.UTF8.GetString(message.Body));
                var distibutionOrder = distibutionOrders.Orders.FirstOrDefault();

                var feedbackOrders = new DomainModels.WearioOrders()
                {
                    DistributorID = distibutionOrders.DistributorID,
                    JobIndentifier = distibutionOrders.JobIndentifier,
                    Orders = distibutionOrders.Orders.Select(distibutionOrder =>
                        new DomainModels.WearioOrder()
                        {
                            ConsolidatedPrice = 0,
                            DistributionOrder = distibutionOrder,
                            FailureReason = message.UserProperties.ContainsKey("ValidationErrors") ? $"{message.UserProperties["ValidationErrors"]}" : "",
                            OrderLines = new List<DomainModels.WearioOrderLine>(),
                            Status = DomainModels.WearioOrdersStatus.FAILED,
                            WearioOrderID = distibutionOrder.WearioOrderId
                        }).ToList()
                };

                _logger.LogInformation("Create feedbackorder for order '{orderID}'", orderID);

                return feedbackOrders;
            }
        }

        /// <summary>
        /// Raises the exceptions that occurred during batch processing
        /// </summary>
        private void RaiseBatchExceptions(List<WearioSynchronizationBatch> batches, Exception exception = null)
        {
            List<Exception> exceptions = new List<Exception>();

            //add the given exception
            if (exception != null)
            {
                //but check if its an AggregateException in which case we want to add all exceptions
                if (exception is AggregateException aggregateException)
                {
                    exceptions.AddRange(aggregateException.InnerExceptions);
                }
                else
                {
                    exceptions.Add(exception);
                }
            }

            //add the exceptions in the batches as well
            if (batches != null)
            {
                exceptions.AddRange(batches.Where(b => b.Exception != null).Select(b => b.Exception));
            }

            //throw when needed
            if (exceptions.Count > 0)
            {
                throw new AggregateException("An error occurred during the synchronisation process.", exceptions);
            }
        }
    }
}
