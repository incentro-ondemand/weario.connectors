using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Weario.Integration.Application.DomainModels;
using Weario.Integration.Application.Configuration;
using Microsoft.Azure.WebJobs.Logging;
using Weario.Integration.Application.ModelDescriptors;

namespace Weario.Integration.Application.Services
{

    /// <summary>
    /// Generic mock data generator
    /// </summary>
    public interface IMockGenerator
    {
        T GenerateMockData<T>(T instance);
    }

    /// <summary>
    /// Generic mock data generator
    /// </summary>
    public class MockGenerator : IMockGenerator
    {
        #region private static readonly string[] Languages 
        private static readonly string[] Languages = new[] { "NL" };
        #endregion
        #region private static readonly string[] MockStrings 
        private static readonly string[] MockStrings = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vitae dignissim eros, sodales euismod dolor. Nullam eget tellus commodo, placerat tortor in, sagittis augue. Cras ac ante eu ex ultrices auctor volutpat dapibus nulla. Praesent condimentum sollicitudin erat eget feugiat. In mollis in purus pretium pellentesque. Mauris in porta elit. Nullam elementum metus sem, sit amet auctor nunc aliquam efficitur. Sed eu ipsum nec neque placerat posuere. Sed ut nulla libero. Sed non condimentum augue. Suspendisse fermentum hendrerit purus. Fusce porta elit id magna faucibus vehicula. Quisque ligula massa, egestas ut urna vel, pulvinar efficitur odio. Aenean a maximus leo.Ut ut aliquet lorem, a cursus ante.Donec ornare condimentum efficitur. Curabitur tincidunt mauris vel mi fermentum scelerisque.Quisque luctus dignissim dui, a lobortis tortor imperdiet sit amet. Vestibulum lacinia arcu sed ligula mattis hendrerit.Pellentesque enim massa, imperdiet quis lorem eu, tempor lobortis ligula.Sed malesuada vehicula ultrices. Sed pulvinar ut purus ut varius. Ut ac neque maximus, blandit nulla nec, hendrerit metus. Curabitur at arcu nulla. Morbi at cursus tellus. Integer porta mi ac tempus facilisis."
    .ToLower()
    .Split(',', '.', ' ')
    .Where(s => s.Length > 4)
    .ToArray();
        #endregion
        #region private static readonly Dictionary<ProductCategory, PrimarySizeType[]> ProductCategorySizes
        private static readonly Dictionary<ProductCategory, PrimarySizeType[]> ProductCategorySizes = new Dictionary<ProductCategory, PrimarySizeType[]>()
            {
                { ProductCategory._3MFILTERS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.AMERICANOVERALLS, new PrimarySizeType[] {  PrimarySizeType.CONFECTIONFEMALE_NL, PrimarySizeType.CONFECTIONMALE_NL } },
                { ProductCategory.ANCHORPOINTS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.ASBESTOSOVERALLS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.BANDEDEARPLUGS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.BASEBALLCAPS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.BELTS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.BLAZERS, new PrimarySizeType[] {  PrimarySizeType.CONFECTIONFEMALE_NL, PrimarySizeType.CONFECTIONMALE_NL, PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.BLOWERSYSTEMS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.BODYWARMERS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.BOOTS, new PrimarySizeType[] {  PrimarySizeType.SHOESIZE_NL } },
                { ProductCategory.CARDIGANS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.CHEMICALRESISTANTGLOVES, new PrimarySizeType[] {  PrimarySizeType.GLOVESIZE_NL } },
                { ProductCategory.CHINOS, new PrimarySizeType[] {  PrimarySizeType.CONFECTIONFEMALE_NL, PrimarySizeType.CONFECTIONMALE_NL, PrimarySizeType.WIDTH_INCHES } },
                { ProductCategory.CLEANINGCLOTHS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.CLOGS, new PrimarySizeType[] {  PrimarySizeType.SHOESIZE_NL } },
                { ProductCategory.COATS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.COMBIFILTERS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.CONSTRUCTIONGLOVES, new PrimarySizeType[] {  PrimarySizeType.GLOVESIZE_NL } },
                { ProductCategory.CORDS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.COSTGUARDPOLOS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.CUTRESISTANTGLOVES, new PrimarySizeType[] {  PrimarySizeType.GLOVESIZE_NL } },
                { ProductCategory.CYCLINGSHORTS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.DRESSES, new PrimarySizeType[] {  PrimarySizeType.CONFECTIONFEMALE_NL, PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.DUSTFILTERS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.DUSTMASKS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.EARMUFFS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.EARPLUGS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.EARPLUGSDISPENCERS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.EYESHOWERS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.EYEWASHBOTTLES, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.EYEWASHSTATION, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.FACESHIELDS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.FALLARRESTBLOCKS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.FALLPROTECTIONCLAMPS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.FILTERCARTRIDGEHOLDERS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.FINEDUSTFILTERS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.FINEDUSTMASKS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.FIRSTAIDKITS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.FIRSTAIDKITSLARGE, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.FIRSTAIDKITSREFILL, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.FIRSTAIDKITSSMALL, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.FLAMERETARDANDCOATS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.FLAMERETARDANDOVERALLS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.FLAMERETARDANDTHERMALSHIRTS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.FLAMERETARDANDWORKPANTS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.FOLDINGMASKS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.FULLFACEMASKS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.GASFILTERS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.GLOVES, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.GOGGLES, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.GRIPGLOVES, new PrimarySizeType[] {  PrimarySizeType.GLOVESIZE_NL } },
                { ProductCategory.HAIRNETCAPS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.HALFFACEMASKS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.HARNESSES, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.HATS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.HEADBANDS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.HELMETS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.HOODYS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.HOSPITALSHIRTS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.HYGIENEKITS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.JACKETS, new PrimarySizeType[] {  PrimarySizeType.CONFECTIONFEMALE_NL, PrimarySizeType.CONFECTIONMALE_NL, PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.JEANS, new PrimarySizeType[] {  PrimarySizeType.WIDTH_INCHES } },
                { ProductCategory.KNEEPADS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.LIGHTOVERALLS, new PrimarySizeType[] {  PrimarySizeType.CONFECTIONFEMALE_NL, PrimarySizeType.CONFECTIONMALE_NL } },
                { ProductCategory.MASKS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.NECKPROTECTORS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.PANTS, new PrimarySizeType[] {  PrimarySizeType.CONFECTIONFEMALE_NL, PrimarySizeType.CONFECTIONMALE_NL, PrimarySizeType.WIDTH_INCHES } },
                { ProductCategory.PARKAS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.PILOTCOATS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.PILOTJACKETS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.PLUGINFIXATION, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.POLOSHIRTS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.POLOSWEATERS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.POSITIONING, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.PULLYS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.QUILTEDCOATS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.RAINCOATS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.RAINOVERALLS, new PrimarySizeType[] {  PrimarySizeType.CONFECTIONFEMALE_NL, PrimarySizeType.CONFECTIONMALE_NL } },
                { ProductCategory.RAINPANTS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.ROPES, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.SAFETYBODYPANTS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.SAFETYCAPS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.SAFETYCOATS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.SAFETYGLOVES, new PrimarySizeType[] {  PrimarySizeType.GLOVESIZE_NL } },
                { ProductCategory.SAFETYGOGGLES, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.SAFETYHATS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.SAFETYHELMETS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.SAFETYLINES, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.SAFETYOVERALLS, new PrimarySizeType[] {  PrimarySizeType.CONFECTIONFEMALE_NL, PrimarySizeType.CONFECTIONMALE_NL } },
                { ProductCategory.SAFETYPANTS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.SAFETYPOLOS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.SAFETYSCREENS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.SAFETYSHOES, new PrimarySizeType[] {  PrimarySizeType.SHOESIZE_NL } },
                { ProductCategory.SAFETYSWEATERS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.SAFETYVESTS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.SCARFS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.SCREWFILTERS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.SHIRTS, new PrimarySizeType[] {  PrimarySizeType.CONFECTIONFEMALE_NL, PrimarySizeType.CONFECTIONMALE_NL, PrimarySizeType.NECKSIZE_CM, PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.SHOEMAINTENANCE, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.SNAPHOOKS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.SOCKS, new PrimarySizeType[] {  PrimarySizeType.SIZERANGE_STRING } },
                { ProductCategory.SOFTSHELLS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.SWEATERS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.SWEATVESTS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.SWIMMINGTRUNKS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.THERMALGLOVES, new PrimarySizeType[] {  PrimarySizeType.GLOVESIZE_NL } },
                { ProductCategory.THERMALPANTS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.THERMALSHIRTS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.THERMALSWEATERS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.TIES, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.TRACKSUITPANTS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.TRAFFICCONTROLERCOATS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.TSHIRTS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.VIOLENCECONTROL, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.WAISTCOATS, new PrimarySizeType[] {  PrimarySizeType.CONFECTIONFEMALE_NL, PrimarySizeType.CONFECTIONMALE_NL, PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.WELDINGGLOVES, new PrimarySizeType[] {  PrimarySizeType.GLOVESIZE_NL } },
                { ProductCategory.WELDINGHOODS, new PrimarySizeType[] {  PrimarySizeType.ONESIZE_BOOLEAN } },
                { ProductCategory.WINTERCOATS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM } },
                { ProductCategory.WINTERGLOVES, new PrimarySizeType[] {  PrimarySizeType.GLOVESIZE_NL } },
                { ProductCategory.WORKINGSHOES, new PrimarySizeType[] {  PrimarySizeType.GLOVESIZE_NL } },
                { ProductCategory.WORKPANTS, new PrimarySizeType[] {  PrimarySizeType.UNISEX_ENUM, PrimarySizeType.UNISEX_ENUM } }
            };
        #endregion

        private ILogger _log;
        private Participants _participants;

        public MockGenerator(
            ILogger<MockGenerator> logger,
            IOptions<Participants> participants)
        {
            _log = logger;
            _participants = participants.Value ?? throw new ArgumentNullException(nameof(participants));
        }

        public T GenerateMockData<T>(T instance)
        {
            return GenerateMockData(instance, null);
        }
        public T GenerateMockData<T>(T instance, IWearioProduct product)
        {
            foreach (var prop in instance.GetType().GetProperties())
            {
                var fragment = this.CreateMockFragment(prop, product);

                //update the language generated data to contain each language once (randomness can generate duplicates)
                //if (fragment is ICollection<LanguageSpecificData>)
                //{
                //    var languageData = fragment as ICollection<LanguageSpecificData>;

                //    int i = 0;
                //    foreach (var language in languageData)
                //    {
                //        //language.LanguageCode = Languages[i++];
                //    }
                //}
                prop.SetValue(instance, fragment);
            }

            return instance;
        }

        private object CreateMockFragment(PropertyInfo property, IWearioProduct product)
        {
            var rnd = new Random();
            Type t = property.PropertyType;

            t = Nullable.GetUnderlyingType(t) ?? t;

            if (property.Name == nameof(SupplierProduct.ProductCategory))
            {
                ProductCategory p;
                do
                {
                    List<ProductCategory> ppc = ProductCategorySizes.Keys.ToList();
                    p = ppc[rnd.Next(ppc.Count)];
                } while ((product as SupplierProduct).Sex == ProductSex.UNISEX && !ProductCategorySizes[p].Any(c => c == PrimarySizeType.UNISEX_ENUM));
                return p;
            }
            else if (property.Name == nameof(SupplierProduct.Sex) && (product as SupplierProduct).ProductCategory.HasValue)
            {
                return ProductCategorySizes[(product as SupplierProduct).ProductCategory.Value].Any(c => c == PrimarySizeType.UNISEX_ENUM)
                    ? ProductSex.UNISEX
                    : RandomEnum(rnd, ProductSex.FEMALE, ProductSex.MALE, ProductSex.UNISEX);
            }
            else if (property.Name == nameof(PostSupplierProducts.SupplierID) && t == typeof(string))
            {
                return _participants.Suppliers.FirstOrDefault()?.ID;
            }
            else if (property.Name == nameof(PostDistributorProducts.DistributorID) && t == typeof(string))
            {
                return _participants.Distributors.FirstOrDefault()?.ID;
            }
            else if (property.Name == nameof(IModelDescriptor.JobIndentifier) && t == typeof(Guid))
            {
                return Guid.NewGuid();
            }
            else if (property.Name == nameof(Image.ImageURL) && t == typeof(string))
            {
                return $"http://domain.com/{this.GetMockString(rnd, 1)}.jpg";
            }
            else if (property.Name == nameof(SupplierLanguageSpecificData.LanguageCode) && t == typeof(string))
            {
                return null; //will be replaced
                //return Languages[rnd.Next(Languages.Length)];
            }
            else if (property.Name == nameof(SupplierLanguageSpecificData.ProductDescription) && t == typeof(string))
            {
                return this.GetMockString(rnd, 15);
            }
            else if (property.Name == nameof(SupplierProduct.EANCode) && t == typeof(long))
            {
                return (long)rnd.Next(Int32.MaxValue);
            }
            if (property.Name == nameof(PostSupplierProducts.SupplierID) || property.Name == nameof(SupplierProduct.SupplierProductID) ||
                property.Name == nameof(PostDistributorProducts.DistributorID) || property.Name == nameof(DistributorProduct.DistributorProductID))
            {
                return Guid.NewGuid().ToString();
            }
            else if (product is SupplierProduct && property.Name == nameof(SupplierColor.Sizes))
            {
                return this.GetSizes(product as SupplierProduct, rnd);
            }
            else if (property.Name == nameof(SupplierColor.ColorHexCode))
            {
                return $"#{rnd.Next(256).ToString("X2")}{rnd.Next(256).ToString("X2")}{rnd.Next(256).ToString("X2")}";
            }
            else if (t == typeof(string))
            {
                return this.GetMockString(rnd);
            }
            else if (t == typeof(int))
            {
                return rnd.Next(100);
            }
            else if (t == typeof(long))
            {
                return (long)rnd.Next(100);
            }
            else if (t == typeof(decimal))
            {
                return rnd.Next(200);
            }
            else if (t == typeof(double))
            {
                return Math.Round(rnd.NextDouble() + rnd.Next(1000), 2);
            }
            else if (t.IsEnum)
            {
                var values = Enum.GetValues(t).Cast<object>().ToArray();
                return values[rnd.Next(values.Length)];
            }
            else if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(ICollection<>))
            {
                var itemType = t.GetGenericArguments()[0];

                var list = this.GetMockInstance(t, product) as IList;

                if (this.IsGeneratedModel(itemType))
                {

                    var count = 30;
                    var minimum = 10;

                    if (property.Name == nameof(DistributorProduct.Colors) ||
                        property.Name == nameof(DistributorSize.PriceRanges) ||
                        property.Name == nameof(SupplierProduct.Colors) ||
                        property.Name == nameof(SupplierProduct.DefaultImages) ||
                        property.Name == nameof(SupplierColor.ColorImages))
                    {
                        minimum = 1;
                        count = 5;
                    }
                    else if (property.Name == "Branches")
                    {
                        minimum = 1;
                        count = 5;
                    }
                    else if (property.Name == "Products")
                    {
                        minimum = count = 10;
                    }
                    else if (property.Name == "LanguageSpecificData")
                    {
                        minimum = Math.Max(Languages.Length - 2, 1); //2 is the random factor
                        count = (Languages.Length - minimum) + 1;
                    }

                    Enumerable
                        .Range(0, rnd.Next(count) + minimum)
                        .Select(i => this.GetMockInstance(itemType, product))
                        .All(o =>
                        {
                            list.Add(o);
                            return true;
                        });
                }

                return list;
            }
            else if (this.IsGeneratedModel(t))
            {
                return this.GetMockInstance(t, product);
            }

            return null;
        }

        private bool IsGeneratedModel(Type type)
        {
            return type.Namespace.StartsWith(typeof(PostSupplierProducts).Namespace);
        }

        private object GetMockInstance(Type type, IWearioProduct product)
        {
            object instance = null;

            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(ICollection<>))
            {
                Type listType = typeof(List<>).MakeGenericType(type.GetGenericArguments()[0]);
                instance = listType.GetConstructor(new Type[0]).Invoke(new object[0]);

            }
            else
            {
                instance = type.GetConstructor(new Type[0]).Invoke(new object[0]);
            }

            if (product == null && type == typeof(SupplierProduct))
            {
                product = (SupplierProduct)instance;
            }

            if (this.IsGeneratedModel(type))
            {
                this.GenerateMockData(instance, product);
            }

            return instance;
        }


        private string GetMockString(Random rnd, int words = 2, bool capitalize = true)
        {
            var value = String.Join(" ", Enumerable
                .Range(0, words)
                .Select(i =>
                {
                    var item = MockStrings[rnd.Next(MockStrings.Length)];
                    if (capitalize && i == 0)
                    {
                        item = System.Globalization.CultureInfo.InvariantCulture.TextInfo.ToTitleCase(item);
                    }
                    return item;
                })
                );

            return value;
        }

        private object GetSizes(SupplierProduct product, Random rnd)
        {
            // Generate a sizes array containing a random amount of sizes where each size has a unique PrimarySizeType/PrimarySizeValue combination.
            List<SupplierSize> sizes = new List<SupplierSize>();
            PrimarySizeType primarySizeType = product.Sex == ProductSex.UNISEX
                ? PrimarySizeType.UNISEX_ENUM
                : ProductCategorySizes[product.ProductCategory.Value][rnd.Next(ProductCategorySizes[product.ProductCategory.Value].Length)];

            if (ProductCategorySizes[product.ProductCategory.Value].Length != 0)
            {
                for (int y = 0; y < rnd.Next(5) + 1; y++)
                {
                    SupplierSize s = null;
                    int i = 0;
                    while (s == null)
                    {
                        s = new SupplierSize()
                        {
                            SupplierProductVariantEANCode = rnd.Next(Int32.MaxValue),
                            SupplierProductVariantID = GetMockString(rnd, 1),
                            PrimarySizeType = primarySizeType
                        };

                        switch (s.PrimarySizeType)
                        {
                            case PrimarySizeType.ONESIZE_BOOLEAN:
                                s.PrimarySizeValue = this.GetMockString(rnd);
                                break;

                            case PrimarySizeType.SIZERANGE_STRING:
                                s.PrimarySizeValue = this.GetMockString(rnd);
                                break;

                            case PrimarySizeType.UNISEX_ENUM:
                                s.PrimarySizeValue = RandomEnum(rnd, "XS", "S", "M", "L", "XL", "XXL", "3XL", "4XL", "5XL");
                                break;

                            case PrimarySizeType.SHOESIZE_NL:
                                s.PrimarySizeValue = (Math.Round(rnd.NextDouble(), 2) + 20).ToString(CultureInfo.InvariantCulture);
                                s.SecondarySizeType = SecondarySizeType.CLASSIFICATION_ENUM;
                                s.SecondarySizeValue = RandomEnum(rnd, "REGULAR", "D", "XD");
                                break;

                            case PrimarySizeType.GLOVESIZE_NL:
                                s.PrimarySizeValue = Math.Round(rnd.NextDouble(), 2).ToString(CultureInfo.InvariantCulture);
                                s.SecondarySizeType = SecondarySizeType.SLEEVELENGTH_INTEGER;
                                s.SecondarySizeValue = RandomEnum(rnd, "5", "7");
                                break;

                            case PrimarySizeType.WIDTH_INCHES:
                                s.PrimarySizeValue = RandomEnum(rnd, "W32", "W36");
                                s.SecondarySizeType = SecondarySizeType.LENGTH_INCHES;
                                s.SecondarySizeValue = RandomEnum(rnd, "L32", "L36");
                                break;

                            case PrimarySizeType.NECKSIZE_CM:
                                s.PrimarySizeValue = (rnd.Next(60) + 20).ToString();
                                s.SecondarySizeType = SecondarySizeType.SLEEVELENGTH_INTEGER;
                                s.SecondarySizeValue = RandomEnum(rnd, "5", "7");
                                break;

                            case PrimarySizeType.CONFECTIONFEMALE_NL:
                            case PrimarySizeType.CONFECTIONMALE_NL:
                                s.PrimarySizeValue = rnd.Next(100).ToString();
                                s.SecondarySizeType = SecondarySizeType.CONFECTIONSIZECATEGORY_ENUM;
                                s.SecondarySizeValue = RandomEnum(rnd, "REGULAR", "CUSTOMSHORT", "CUSTOMLONG", "CUSTOMWAIST");
                                switch (s.SecondarySizeValue)
                                {
                                    case "REGULAR": s.PrimarySizeValue = "28"; break;
                                    case "CUSTOMSHORT": s.PrimarySizeValue = "44"; break;
                                    case "CUSTOMLONG": s.PrimarySizeValue = "35"; break;
                                    case "CUSTOMWAIST": s.PrimarySizeValue = "612"; break;
                                }
                                break;
                            default:
                                throw new Exception("Size " + s.PrimarySizeType + " not configured!");
                        }

                        if (sizes.Exists(p => p.PrimarySizeType == s.PrimarySizeType && p.PrimarySizeValue == s.PrimarySizeValue))
                        {
                            s = null;
                            if (++i > 100) break;
                        }
                    }
                    sizes.Add(s);
                }
            }
            return sizes;
        }

        //private object GetSizes(DistributorProduct product, Random rnd)
        //{
        //    List<DistributorProduct> sizes = new List<DistributorProduct>();

        //    return sizes;
        //}

        private T RandomEnum<T>(Random rnd, params T[] items)
        {
            return items[rnd.Next(items.Length)];
        }
    }
}
