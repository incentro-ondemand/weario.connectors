﻿using Microsoft.Azure.Cosmos.Table;
using Microsoft.Azure.WebJobs.Logging;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Weario.Integration.Application.Configuration;
using Weario.Integration.Application.DomainModels;

namespace Weario.Integration.Application.Services
{
    public interface ISynchronisationService
    {
        Task<SynchronisationEntity> GetSynchronisation(ParticipantGroup participantGroup, Guid participantID);

        Task<SynchronisationEntity> UpsertSynchronisation(ParticipantGroup participantGroup, Guid participantID, Guid jobID, DateTime startDate);
    }

    public class SynchronisationService : ISynchronisationService
    {
        private readonly ILogger _log;
        private readonly Participants _participants;
        private readonly IModelDescriptorFactory _descriptorFactory;
        private readonly GeneralSettings _appSettings;
        private CloudTable _table;

        /// <summary>
        /// Constructor
        /// </summary>
        public SynchronisationService(
            ILogger<SynchronisationService> logger,
            IOptions<Participants> participants,
            IModelDescriptorFactory descriptorFactory,
            IOptions<GeneralSettings> appSettings)
        {
            _log = logger;
            _participants = participants.Value ?? throw new ArgumentNullException(nameof(participants));
            _descriptorFactory = descriptorFactory ?? throw new ArgumentNullException(nameof(descriptorFactory));
            _appSettings = appSettings.Value ?? throw new ArgumentNullException(nameof(appSettings));
        }

        public async Task<CloudTable> Table()
        {
            if (_table == null)
            {
                string storageConnectionString = _appSettings.Storage.ConnectionString;
                string tableName = _appSettings.Storage.TableSynchronisation;

                // Retrieve storage account information from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storageConnectionString);

                // Create a table client for interacting with the table service
                CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

                // Create a table client for interacting with the table service 
                _table = tableClient.GetTableReference(tableName);
                if (await _table.CreateIfNotExistsAsync())
                {
                    _log.LogInformation("Created Table named: {0}", tableName);
                }
                else
                {
                    _log.LogInformation("Table {0} already exists", tableName);
                }
            }
            return _table;
        }

        public async Task<SynchronisationEntity> GetSynchronisation(ParticipantGroup participantGroup, Guid participantID)
        {
            // Connect to storage table
            var table = await this.Table();

            var condition = TableQuery.CombineFilters(
                TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, participantGroup.ToString()),
                TableOperators.And,
                TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, participantID.ToString())
            );
            var query = new TableQuery<SynchronisationEntity>().Where( condition );
            var lst = table.ExecuteQuery(query);

            return lst.FirstOrDefault();
        }

        public async Task<SynchronisationEntity> UpsertSynchronisation(ParticipantGroup participantGroup, Guid participantID, Guid jobID, DateTime startDate)
        {
            if (participantID == null && participantID == Guid.Empty ) throw new ArgumentNullException("participantID");

            var synchronisationEntity = new SynchronisationEntity(participantGroup, participantID)
            {
                JobID = jobID,
                StartDate = startDate
            };

            try
            {
                // Create the InsertOrReplace table operation
                TableOperation insertOrMergeOperation = TableOperation.InsertOrMerge(synchronisationEntity);

                // Connect to storage table
                var table = await this.Table();

                // Execute the operation.
                TableResult result = await _table.ExecuteAsync(insertOrMergeOperation);
                SynchronisationEntity insertedEntity = result.Result as SynchronisationEntity;

                return insertedEntity;
            }
            catch (StorageException ex)
            {
                _log.LogError(ex, ex.Message);
                throw;
            }
        }
    }
}
