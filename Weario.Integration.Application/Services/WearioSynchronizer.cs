﻿using Dasync.Collections;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.ServiceBus.Core;
using Microsoft.Azure.ServiceBus.Management;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Weario.Integration.Application.Configuration;
using Weario.Integration.Application.Extensions;
using Weario.Integration.Application.ModelDescriptors;
using Weario.Integration.Infrastructure.Clients.Weario;

namespace Weario.Integration.Application.Services
{
    /// <summary>
    /// Interface that describes the synchronizer class that can synchronize message batches to Weario
    /// </summary>
    public interface IWearioSynchronizer
    {
        Task ProcessRequest<T>(T model) where T : IModelDescriptor;
        Task SendAllToWeario<T>(Func<IEnumerable<T>, WearioApiClient, Task> sendFunction, CancellationToken cancellationToken) where T : IModelDescriptor;
    }

    /// <summary>
    /// Class that can synchronize message batches to Weario
    /// </summary>
    public class WearioSynchronizer : IWearioSynchronizer
    {
        private ILogger _logger;
        private readonly IMessageSender _sender;
        private readonly IServiceBusManager _serviceBusManager;
        private readonly IModelDescriptorFactory _descriptorFactory;
        private readonly GeneralSettings _appSettings;
        private readonly WearioApiClient _wearioApiClient;
        private readonly Participants _participants;

        /// <summary>
        /// Constructor
        /// </summary>
        public WearioSynchronizer(
            ILogger<WearioSynchronizer> logger,
            IMessageSender sender,
            IServiceBusManager serviceBusManager,
            IModelDescriptorFactory descriptorFactory,
            IOptions<GeneralSettings> appSettings,
            WearioApiClient supplierClient,
            IOptions<Participants> participants
            )
        {
            _logger = logger;
            _sender = sender ?? throw new ArgumentNullException(nameof(sender));
            _serviceBusManager = serviceBusManager ?? throw new ArgumentNullException(nameof(serviceBusManager));
            _descriptorFactory = descriptorFactory ?? throw new ArgumentNullException(nameof(descriptorFactory));
            _appSettings = appSettings.Value ?? throw new ArgumentNullException(nameof(appSettings));
            _wearioApiClient = supplierClient;
            _participants = participants.Value ?? throw new ArgumentNullException(nameof(participants));
        }

        /// <summary>
        /// Processes incoming requests. These requests are split up into separate items and placed on the service bus for delivery to destination.
        /// </summary>
        public async Task ProcessRequest<T>(T model) where T : IModelDescriptor
        {
            var descriptor = _descriptorFactory.Create<T>();
            descriptor.Model = model;
            descriptor.ParticipantID = descriptor.ParticipantID.ToLower();

            // Generate indentifier for this synchronisation run.
            if (descriptor.JobIndentifier.Equals(Guid.Empty))
            {
                descriptor.JobIndentifier = Guid.NewGuid();
            }

            //process the message
            using (_logger.BeginScope("{JobIndentifier}{Topic}", descriptor.JobIndentifier, descriptor.Topic))
            {
                try
                {
                    _logger.LogInformation("Processing {messageType} message.", typeof(T).Name);

                    //make sure the subscription exists
                    await _serviceBusManager.EnsureParticipantSubscription<T>(descriptor.ParticipantID);

                    var jsonSerializerSettings = new JsonSerializerSettings()
                    {
                        NullValueHandling = NullValueHandling.Ignore,
                        ContractResolver = new DefaultContractResolver
                        {
                            NamingStrategy = new CamelCaseNamingStrategy()
                        }
                    };

                    //check if we need to split up the message
                    if (descriptor.SendIndividually)
                    {
                        //split the model up in to seperate items
                        var models = descriptor.CreateModelPerItem();

                        //create a list of serialized messages (we want to split up all items so we can process, send and/or fail them individually)
                        var serializedMessages = models.Select(m =>
                            _sender.CreateMessage(
                                $"{descriptor.MessageKeyID}:{descriptor.MessageKeyValue(m)}",
                                JsonConvert.SerializeObject(m, jsonSerializerSettings),
                                new Dictionary<string, object> {
                                    { descriptor.ParticipantKeyField, descriptor.ParticipantID },
                                    { nameof(descriptor.JobIndentifier), descriptor.JobIndentifier },
                                    { descriptor.MessageKeyID, descriptor.MessageKeyValue(m) }
                                }
                            )
                        ).ToList();

                        //put each item on the service bus
                        await _sender.SendMessages(descriptor.Topic, serializedMessages);
                    }
                    else
                    {
                        //serialize again
                        string serializedMessage = JsonConvert.SerializeObject(descriptor.Model, jsonSerializerSettings);

                        //send the mssage
                        await _sender.SendMessage(
                            descriptor.Topic,
                            $"{descriptor.MessageKeyID}:{descriptor.MessageKeyValue(descriptor.Model)}",
                            serializedMessage,
                            new Dictionary<string, object> {
                                { descriptor.ParticipantKeyField, descriptor.ParticipantID },
                                { nameof(descriptor.JobIndentifier), descriptor.JobIndentifier },
                                { descriptor.MessageKeyID, descriptor.MessageKeyValue(descriptor.Model) }
                            }
                        );
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex);

                    throw new Exception("Could not process message.", ex);
                }
            }
        }

        public async Task SendAllToWeario<T>(Func<IEnumerable<T>, WearioApiClient, Task> sendFunction, CancellationToken cancellationToken) where T : IModelDescriptor
        {
            //create the descriptor for the given type. This way we can process different types in a singular manner.
            ModelDescriptorBase<T> descriptor = _descriptorFactory.Create<T>();

            using var scope = _logger.BeginScope("{Topic}", descriptor.Topic);

            int totalMessagesProcessed = 0;
            try
            {
                _logger.LogInformation("Start sending messages to Weario.");

                int messagesProcessed = 0;
                do
                {
                    messagesProcessed = await SendToWeario(sendFunction, cancellationToken, descriptor);
                    totalMessagesProcessed += messagesProcessed;

                    _logger.LogInformation("Sent {messagesProcessed} message(s) to Weario.", messagesProcessed);
                }
                while (cancellationToken.IsCancellationRequested == false && messagesProcessed > 0);

                _logger.LogInformation("Processed {totalMessagesProcessed} messages for Weario.", totalMessagesProcessed);
            }
            catch
            {
                _logger.LogInformation("Processed {totalMessagesProcessed} messages for Weario. Operation cancelled: {cancelled}.", totalMessagesProcessed, cancellationToken.IsCancellationRequested);
                throw;
            }
            finally
            {
                _logger.LogInformation("Finisched sending messages to Weario.");
            }
        }

        /// <summary>
        /// Sends processed items that are placed on the service bus to destination.
        /// </summary>
        private async Task<int> SendToWeario<T>(
            Func<IEnumerable<T>, WearioApiClient, Task> sendFunction,
            CancellationToken cancellationToken,
            ModelDescriptorBase<T> descriptor
            ) where T : IModelDescriptor
        {
            _logger.LogInformation("Send messages to Weario {messageType}.", typeof(T).Name);

            //make sure we have a subscription for each participant
            await _serviceBusManager.EnsureParticipantsSubscriptions<T>();

            ManagementClient manager = null;
            List<WearioSynchronizationBatch> batches = null;
            int messageCount = 0;

            try
            {
                //create the manager
                manager = new ManagementClient(_appSettings.ServiceBus.ConnectionString);

                //prepare topic for message retrieval (create subscriptions, messagerecievers)
                try
                {
                    _logger.LogInformation("Start preparing topic for message retrieval.");

                    //get the subscriptions
                    var subscriptions = await manager.GetSubscriptionsAsync(descriptor.Topic);


                    var participantSubscriptionNames = _participants.Distributors.Select(p => _serviceBusManager.GetParticipantSubscriptionName(p)).ToList();
                        participantSubscriptionNames.AddRange(_participants.Suppliers.Select(p => _serviceBusManager.GetParticipantSubscriptionName(p)).ToList());                       

                    //prepare the batch retrieval
                    batches = subscriptions
                        .Where(s=> participantSubscriptionNames.Contains(s.SubscriptionName))
                        .Select(subscription => new WearioSynchronizationBatch(
                            new MessageReceiver(
                                _appSettings.ServiceBus.ConnectionString,
                                EntityNameHelper.FormatSubscriptionPath(subscription.TopicPath, subscription.SubscriptionName)
                            ),
                            subscription
                        ))
                        .ToList();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex);

                    throw new Exception("Could not retrieve topic subscriptions.", ex);
                }
                finally
                {
                    _logger.LogInformation("Finished preparing topic for message retrieval.");
                }

                //retrieve messages from the subscripions
                try
                {
                    _logger.LogInformation("Start retrieving message batches from the topic. Batchcount: {Batchcount}", batches.Count);

                    //retrieve results in parallel
                    await batches.ParallelForEachAsync(async (batch) =>
                    {
                        try
                        {
                            batch.Results = await batch.Receiver.ReceiveAsync(descriptor.SyncBatchSize, _appSettings.SyncTimeout) ?? new List<Message>();
                        }
                        catch (Exception ex)
                        {
                            batch.Exception = ex;
                        }
                    }, _appSettings.ServiceBus.MaxParallelism);

                    //determine results
                    int successCount = batches.Where(b => b.Exception == null && b.Results.Count > 0).Count();
                    int emptyCount = batches.Where(b => b.Exception == null && b.Results.Count == 0).Count();
                    int errorCount = batches.Where(b => b.Exception != null).Count();
                    messageCount = batches.Where(b => b.Exception == null).Sum(b => b.Results.Count());

                    _logger.LogInformation("Found {successBatchCount} successful batches, {emptyBatchCount} empty batches, {errorBatchCount} failed batches, {messageCount} message(s) in total.",
                        successCount,
                        emptyCount,
                        errorCount,
                        messageCount
                        );
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex);

                    throw new Exception("Could not retrieve message(s).", ex);
                }
                finally
                {
                    _logger.LogInformation("Finished retrieving message batches from the topic.");
                }

                //check if we have to send anything
                if (messageCount > 0)
                {
                    //process results in parallel
                    await batches
                        .Where(b => b.Exception == null && b.Results.Count > 0)
                        .ToList()
                        .ParallelForEachAsync(async (batch) =>
                    {
                        var ParticipantID = batch.Results.Select(m => m.UserProperties[descriptor.ParticipantKeyField]).Cast<string>().FirstOrDefault();

                        using (_logger.BeginScope("{" + descriptor.ParticipantKeyField + "}", ParticipantID))
                        {
                            try
                            {
                                _logger.LogInformation("Starting sending batch containing {messageCount} message(s) to weario.", batch.Results.Count, batch.Subscription.SubscriptionName);

                                //the given list of messages should all be of the same participant
                                if (batch.Results.GroupBy(m => m.UserProperties[descriptor.ParticipantKeyField]).Count() > 1)
                                {
                                    throw new Exception("Can't send message to Weario because the batch contains messages of different participants.");
                                }

                                //send the batch to weario
                                if (_appSettings.WearioApi.Mock)
                                {
                                    //complete the messages
                                    await batch.Receiver.CompleteAsync(batch.Results.Select(m => m.SystemProperties.LockToken));
                                }
                                else
                                {
                                    await DeliverBatch(sendFunction, batch, descriptor);
                                }
                            }
                            catch (Exception ex)
                            {
                                //Exception will be thrown after all batches are done.
                                batch.Exception = ex;

                                //cancel messages 
                                foreach (var message in batch.Results)
                                {
                                    await batch.Receiver.AbandonAsync(message.SystemProperties.LockToken, message.UserProperties);
                                }
                            }
                            finally
                            {
                                _logger.LogInformation("Finished sending batch to weario.");
                            }
                        }
                    }, _appSettings.WearioApi.MaxParallelism);
                }
            }
            catch (Exception ex)
            {
                this.RaiseBatchExceptions(batches, ex);
            }
            finally
            {
                manager?.CloseAsync();

                if (batches != null)
                {
                    Task.WaitAll(batches.Select(b => b.Receiver.CloseAsync()).ToArray());
                }
            }

            this.RaiseBatchExceptions(batches);

            return messageCount;
        }

        private async Task DeliverBatch<T>(Func<IEnumerable<T>, WearioApiClient, Task> sendFunction, WearioSynchronizationBatch batch, ModelDescriptorBase<T> descriptor) where T : IModelDescriptor
        {
            var batchResults = batch.Results.ToList();
            while (batchResults.Count > 0)
            {
                var requestMessages = batchResults.Take(_appSettings.WearioApi.ItemsPerRequest);

                try
                {
                    _logger.LogInformation("Start sending {count} message(s) to Weario.", requestMessages.Count());

                    await sendFunction(
                        requestMessages.Select(m => JsonConvert.DeserializeObject<T>(Encoding.UTF8.GetString(m.Body))),
                        _wearioApiClient
                    );

                    //complete the messages
                    await batch.Receiver.CompleteAsync(requestMessages.Select(m => m.SystemProperties.LockToken));

                    // Log successful message for countability 
                    foreach (var message in requestMessages)
                    {
                        // Retrieve productid
                        string productID = message.UserProperties[descriptor.MessageKeyID] as string;
                        string JobIndentifier = message.UserProperties["JobIndentifier"] as string;

                        _logger.LogInformation("{Delivery} delivery for {ProductID}. Job:{JobIndentifier}.", "Successful", productID, JobIndentifier);
                    }
                }
                catch (ApiException<ValidationErrorsResponse> ex)
                {
                    // request contains invalid products
                    _logger.LogInformation("Message rejected. Validation Failed:");

                    await ProcessDeliverBatchException(batch, descriptor, requestMessages,
                        (string productID) =>
                        {
                            // get errors for product id
                            var errors = ex.Result.ValidationErrors.Where(e => string.Equals(e.ValidationContext, productID));
                            if (errors.Count() == 0) errors = ex.Result.ValidationErrors;

                            if (errors.Count() > 0)
                            {
                                // combine validation errors for productid
                                return string.Join(",", errors.Select(e => $"P:{e.ValidationContext}, F:{e.FieldName}, M:{e.Message}"));
                            }
                            return null;
                        }
                    );
                }
                catch (ApiException ex)
                {
                    // Exception 
                    _logger.LogInformation("Message rejected.");

                    await ProcessDeliverBatchException(batch, descriptor, requestMessages, (string productID) => ex.Message);
                }
                finally
                {
                    _logger.LogInformation("Finished sending message(s) to Weario.");
                }

                batchResults.RemoveRange(0, requestMessages.Count());
            }
            batch.Results = batchResults;
        }

        private async Task ProcessDeliverBatchException<T>(
            WearioSynchronizationBatch batch,
            ModelDescriptorBase<T> descriptor,
            IEnumerable<Message> requestMessages,
            Func<string, string> getErrorMessage) where T : IModelDescriptor
        {
            foreach (var message in requestMessages)
            {
                // Retrieve productid
                string productID = message.UserProperties[descriptor.MessageKeyID] as string;
                string JobIndentifier = message.UserProperties["JobIndentifier"] as string;
                int cCount = (message.UserProperties.TryGetValue("RedeliveryCounter", out object oCount) ? (int)oCount : 0);

                using (_logger.BeginScope("{ProductID}{JobIndentifier}{DeliveryCount}", productID, JobIndentifier, cCount))
                {
                    var errorMessage = getErrorMessage(productID);

                    if (string.IsNullOrWhiteSpace(errorMessage) == false)
                    {
                        _logger.LogWarning("ErrorMessage: {errorMessage}.", errorMessage);

                        message.UserProperties["ValidationErrors"] = errorMessage;
                    }
                    await ProcessMessageException(descriptor, batch.Subscription, batch.Receiver, message, cCount);
                }
            }
        }

        private async Task ProcessMessageException<T>(ModelDescriptorBase<T> descriptor, SubscriptionDescription subscription, MessageReceiver receiver, Message message, int cCount) where T : IModelDescriptor
        {
            // handle service bus message acknowledgement
            if ((cCount + 1) >= subscription.MaxDeliveryCount)
            {
                await receiver.DeadLetterAsync(message.SystemProperties.LockToken, message.UserProperties);
            }
            else
            {
                var newMsg = message.Clone();
                newMsg.UserProperties["RedeliveryCounter"] = cCount + 1;

                await _sender.ScheduleMessage(subscription.TopicPath, newMsg, DateTimeOffset.UtcNow.AddSeconds(Math.Abs(_appSettings.ServiceBus.RedeliveryDelaySeconds)));
                await receiver.CompleteAsync(message.SystemProperties.LockToken);

                _logger.LogInformation("Unsuccessful delivery. Try:{trycount}", cCount + 1);
            }
        }

        /// <summary>
        /// Raises the exceptions that occurred during batch processing
        /// </summary>
        private void RaiseBatchExceptions(List<WearioSynchronizationBatch> batches, Exception exception = null)
        {
            List<Exception> exceptions = new List<Exception>();

            //add the given exception
            if (exception != null)
            {
                //but check if its an AggregateException in which case we want to add all exceptions
                if (exception is AggregateException aggregateException)
                {
                    exceptions.AddRange(aggregateException.InnerExceptions);
                }
                else
                {
                    exceptions.Add(exception);
                }
            }

            //add the exceptions in the batches as well
            if (batches != null)
            {
                exceptions.AddRange(batches.Where(b => b.Exception != null).Select(b => b.Exception));
            }

            //throw when needed
            if (exceptions.Count > 0)
            {
                throw new AggregateException("An error occurred during the synchronisation process.", exceptions);
            }
        }
    }
}
