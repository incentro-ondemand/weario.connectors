﻿using Dasync.Collections;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.ServiceBus.Core;
using Microsoft.Azure.ServiceBus.Management;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Weario.Integration.Application.Configuration;
using Weario.Integration.Application.Extensions;
using Weario.Integration.Application.ModelDescriptors;
using Weario.Integration.Infrastructure.Clients;
using Weario.Integration.Infrastructure.Clients.Distributor;

namespace Weario.Integration.Application.Services
{

    public interface IOrderManager
    {
        Task<DomainModels.DistributorOrders> OrdersForDistributorAsync(string distributorId, int maxOrders);
        Task SendAllToDistributor<T>(Func<IEnumerable<T>, DistributorApiClient, Task> sendFunction, CancellationToken cancellationToken) where T : IModelDescriptor;
    }

    public class OrderManager : IOrderManager
    {
        private readonly ILogger _logger;
        private readonly IServiceBusManager _serviceBusManager;
        private readonly GeneralSettings _appSettings;
        private readonly Participants _participants;
        private readonly IMessageSender _sender;
        private readonly IModelDescriptorFactory _descriptorFactory;
        private readonly IBaseHttpClient _baseHttpClient;

        /// <summary>
        /// Constructor
        /// </summary>
        public OrderManager(
            ILogger<OrderManager> logger,
            IServiceBusManager serviceBusManager,
            IOptions<GeneralSettings> appSettings,
            IOptions<Participants> participants,
            IMessageSender sender,
            IModelDescriptorFactory descriptorFactory,
            IBaseHttpClient baseHttpClient)
        {
            _logger = logger;
            _serviceBusManager = serviceBusManager ?? throw new ArgumentNullException(nameof(serviceBusManager));
            _appSettings = appSettings.Value ?? throw new ArgumentNullException(nameof(appSettings));
            _participants = participants.Value ?? throw new ArgumentNullException(nameof(participants));
            _sender = sender;
            _descriptorFactory = descriptorFactory;
            _baseHttpClient = baseHttpClient;

        }

        public async Task<DomainModels.DistributorOrders> OrdersForDistributorAsync(string distributorId, int maxOrders)
        {
            // Lookup distributor using the distributorId
            var distributor = _participants.Distributors.FirstOrDefault(p => p.ID.Equals(distributorId, StringComparison.InvariantCultureIgnoreCase));

            // Validate distributorId
            if (distributorId == null)
            {
                throw new Exception($"Distributor with Id {distributorId} is not configured.");
            }

            _logger.LogInformation($"Retrieve orders for distributor {distributor.ID}.");

            // Create return object
            var distributorOrders = new DomainModels.DistributorOrders()
            {
                DistributorID = distributorId
            };

            // Make sure topic subscriptions exists for the order retrieval
            await _serviceBusManager.EnsureParticipantSubscription<DomainModels.DistributorOrders>(distributorId);

            return distributorOrders;
        }

        public async Task SendAllToDistributor<T>(Func<IEnumerable<T>, DistributorApiClient, Task> sendFunction, CancellationToken cancellationToken) where T : IModelDescriptor
        {
            //create the descriptor for the given type. This way we can process different types in a singular manner.
            ModelDescriptorBase<T> descriptor = _descriptorFactory.Create<T>();

            using var scope = _logger.BeginScope("{Topic}", descriptor.Topic);

            int totalMessagesProcessed = 0;
            try
            {
                _logger.LogInformation("Start sending orders to distributor.");

                int messagesProcessed = 0;
                do
                {
                    messagesProcessed = await SendToDistributor(sendFunction, cancellationToken, descriptor);
                    totalMessagesProcessed += messagesProcessed;

                    _logger.LogInformation("Sent {messagesProcessed} orders to Distributor.", messagesProcessed);
                }
                while (cancellationToken.IsCancellationRequested == false && messagesProcessed > 0);

                _logger.LogInformation("Processed {totalMessagesProcessed} orders for Distributor.", totalMessagesProcessed);
            }
            catch
            {
                _logger.LogInformation("Processed {totalMessagesProcessed} orders for Distributor. Operation cancelled: {cancelled}.", totalMessagesProcessed, cancellationToken.IsCancellationRequested);
                throw;
            }
            finally
            {
                _logger.LogInformation("Finisched sending orders to distributor.");
            }
        }

        /// <summary>
        /// Sends processed items that are placed on the service bus to destination.
        /// </summary>
        private async Task<int> SendToDistributor<T>(
            Func<IEnumerable<T>, DistributorApiClient, Task> sendFunction,
            CancellationToken cancellationToken,
            ModelDescriptorBase<T> descriptor
            ) where T : IModelDescriptor
        {
            _logger.LogInformation("Send messages to distributor {messageType}.", typeof(T).Name);

            //make sure we have a subscription for each participant
            await _serviceBusManager.EnsureParticipantsSubscriptions<T>();

            ManagementClient manager = null;
            List<WearioSynchronizationBatch> batches = null;
            int messageCount = 0;

            try
            {
                //create the manager
                manager = new ManagementClient(_appSettings.ServiceBus.ConnectionString);

                //prepare topic for message retrieval (create subscriptions, messagerecievers)
                try
                {
                    _logger.LogInformation("Start preparing topic for message retrieval.");

                    //get the subscriptions
                    var subscriptions = await manager.GetSubscriptionsAsync(descriptor.Topic);

                    // Only select destributors where api delivbery is configured.
                    var subscriptionNames = _participants.Distributors.Where(d => d.OrderApi?.IsValid ?? false).Select(d => _serviceBusManager.GetParticipantSubscriptionName(d)).ToList();
                    subscriptions = subscriptions.Where(s => subscriptionNames.Contains(s.SubscriptionName)).ToList();

                    //prepare the batch retrieval
                    batches = subscriptions
                        .Select(subscription => new WearioSynchronizationBatch(
                            new MessageReceiver(
                                _appSettings.ServiceBus.ConnectionString,
                                EntityNameHelper.FormatSubscriptionPath(subscription.TopicPath, subscription.SubscriptionName),
                                ReceiveMode.PeekLock
                            ),
                            subscription
                        ))
                        .ToList();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex);

                    throw new Exception("Could not retrieve topic subscriptions.", ex);
                }
                finally
                {
                    _logger.LogInformation("Finished preparing topic for message retrieval.");
                }

                //retrieve messages from the subscripions
                try
                {
                    _logger.LogInformation("Start retrieving message batches from the topic. Batchcount: {Batchcount}", batches.Count);

                    //retrieve results in parallel
                    await batches.ParallelForEachAsync(async (batch) =>
                    {
                        try
                        {
                            batch.Results = await batch.Receiver.ReceiveAsync(descriptor.SyncBatchSize, _appSettings.SyncTimeout) ?? new List<Message>();
                        }
                        catch (Exception ex)
                        {
                            batch.Exception = ex;
                        }
                    }, _appSettings.ServiceBus.MaxParallelism);

                    //determine results
                    int successCount = batches.Where(b => b.Exception == null && b.Results.Count > 0).Count();
                    int emptyCount = batches.Where(b => b.Exception == null && b.Results.Count == 0).Count();
                    int errorCount = batches.Where(b => b.Exception != null).Count();
                    messageCount = batches.Where(b => b.Exception == null).Sum(b => b.Results.Count());

                    _logger.LogInformation("Found {successBatchCount} successful batches, {emptyBatchCount} empty batches, {errorBatchCount} failed batches, {messageCount} message(s) in total.",
                        successCount,
                        emptyCount,
                        errorCount,
                        messageCount
                    );
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex);

                    throw new Exception("Could not retrieve message(s).", ex);
                }
                finally
                {
                    _logger.LogInformation("Finished retrieving message batches from the topic.");
                }

                //check if we have to send anything
                if (messageCount > 0)
                {
                    //process results in parallel
                    await batches
                        .Where(b => b.Exception == null && b.Results.Count > 0)
                        .ToList()
                        .ParallelForEachAsync(async (batch) =>
                    {
                        var ParticipantID = batch.Results.Select(m => m.UserProperties[descriptor.ParticipantKeyField]).Cast<string>().FirstOrDefault();

                        using (_logger.BeginScope("{" + descriptor.ParticipantKeyField + "}", ParticipantID))
                        {
                            try
                            {
                                _logger.LogInformation("Starting sending batch containing {messageCount} message(s) to distributor.", batch.Results.Count, batch.Subscription.SubscriptionName);

                                //the given list of messages should all be of the same participant
                                if (batch.Results.GroupBy(m => m.UserProperties[descriptor.ParticipantKeyField]).Count() > 1)
                                {
                                    throw new Exception("Can't send orders to distributor because the batch contains messages of different participants.");
                                }

                                //send the batch to weario
                                if (_appSettings.WearioApi.Mock)
                                {
                                    //complete the messages
                                    await batch.Receiver.CompleteAsync(batch.Results.Select(m => m.SystemProperties.LockToken));
                                }
                                else
                                {
                                    await DeliverBatch(sendFunction, batch, descriptor);
                                }
                            }
                            catch (Exception ex)
                            {
                                //Exception will be thrown after all batches are done.
                                batch.Exception = ex;

                                //cancel messages 
                                foreach (var message in batch.Results)
                                {
                                    await batch.Receiver.AbandonAsync(message.SystemProperties.LockToken, message.UserProperties);
                                }
                            }
                            finally
                            {
                                _logger.LogInformation("Finished sending batch to distributor.");
                            }
                        }
                    }, _appSettings.WearioApi.MaxParallelism);
                }
            }
            catch (Exception ex)
            {
                this.RaiseBatchExceptions(batches, ex);
            }
            finally
            {
                manager?.CloseAsync();

                if (batches != null)
                {
                    Task.WaitAll(batches.Select(b => b.Receiver.CloseAsync()).ToArray());
                }
            }

            this.RaiseBatchExceptions(batches);

            return messageCount;
        }

        private async Task DeliverBatch<T>(Func<IEnumerable<T>, DistributorApiClient, Task> sendFunction, WearioSynchronizationBatch batch, ModelDescriptorBase<T> descriptor) where T : IModelDescriptor
        {
            Distributor distributor = _participants.Member(descriptor.Group, batch.Results.First().UserProperties[descriptor.ParticipantKeyField].ToString()) as Distributor;
            DistributorApiClient client = GetApiClient(distributor?.OrderApi);
            if (client == null)
            {
                var msg = string.IsNullOrWhiteSpace(distributor?.OrderApi?.Url) ? "configured" : "enabled";

                _logger.LogInformation($"Connection failure. Endpoint not {msg}:");
                foreach (var message in batch.Results)
                {
                    _logger.LogInformation("- Product {productID}.", message.UserProperties[descriptor.MessageKeyID]);

                    message.UserProperties["ValidationErrors"] = $"Endpoint not {msg}";

                    await batch.Receiver.DeadLetterAsync(message.SystemProperties.LockToken, message.UserProperties);
                }
            }
            else
            {
                var batchResults = batch.Results.ToList();
                while (batchResults.Count > 0)
                {
                    var requestMessages = batchResults.Take(_appSettings.WearioApi.ItemsPerRequest);

                    try
                    {
                        _logger.LogInformation("Start sending {count} message(s) to distributor.", requestMessages.Count());

                        await sendFunction(
                            requestMessages.Select(m => JsonConvert.DeserializeObject<T>(Encoding.UTF8.GetString(m.Body))),
                            client
                        );

                        //complete the messages
                        await batch.Receiver.CompleteAsync(requestMessages.Select(m => m.SystemProperties.LockToken));

                        // Log successful message for countability 
                        foreach (var message in requestMessages)
                        {
                            // Retrieve productid
                            string productID = message.UserProperties[descriptor.MessageKeyID] as string;
                            string JobIndentifier = message.UserProperties["JobIndentifier"] as string;

                            _logger.LogInformation("{Delivery} delivery for {ProductID}. Job:{JobIndentifier}.", "Successful", productID, JobIndentifier);
                        }
                    }
                    catch (InvalidOperationException ex)
                    {
                        // Connection failure
                        _logger.LogInformation("Connection failure:");

                        await ProcessDeliverBatchException(batch, descriptor, requestMessages, (string productID) => ex.Message);
                    }
                    catch (ApiException<Integration.Infrastructure.Clients.Weario.ValidationErrorsResponse> ex)
                    {
                        // request contains invalid products
                        _logger.LogInformation("Message rejected. Validation Failed:");

                        await ProcessDeliverBatchException(batch, descriptor, requestMessages, (string productID) =>
                        {
                            var errors = ex.Result.ValidationErrors.Where(e => string.Equals(e.ValidationContext, productID));
                            if (errors.Count() > 0)
                            {
                                // combine validation errors for productid
                                return string.Join(",", errors.Select(e => $"P:{e.ValidationContext}, F:{e.FieldName}, M:{e.Message}"));
                            }
                            return null;
                        });
                    }
                    finally
                    {
                        _logger.LogInformation("Finished sending order(s) to distributor.");
                    }

                    batchResults.RemoveRange(0, requestMessages.Count());
                }
                batch.Results = batchResults;
            }
        }
        private async Task ProcessDeliverBatchException<T>(
            WearioSynchronizationBatch batch,
            ModelDescriptorBase<T> descriptor,
            IEnumerable<Message> requestMessages,
            Func<string, string> getErrorMessage) where T : IModelDescriptor
        {
            foreach (var message in requestMessages)
            {
                // Retrieve productid
                string productID = message.UserProperties[descriptor.MessageKeyID] as string;
                string JobIndentifier = message.UserProperties["JobIndentifier"] as string;
                int cCount = (message.UserProperties.TryGetValue("RedeliveryCounter", out object oCount) ? (int)oCount : 0);

                using (_logger.BeginScope("{ProductID}{JobIndentifier}{DeliveryCount}", productID, JobIndentifier, cCount))
                {
                    var errorMessage = getErrorMessage(productID);

                    if (string.IsNullOrWhiteSpace(errorMessage) == false)
                    {
                        _logger.LogWarning("ErrorMessage: {errorMessage}.", errorMessage);

                        message.UserProperties["ValidationErrors"] = errorMessage;
                    }
                    await ProcessMessageException(batch.Subscription, batch.Receiver, message, cCount);
                }
            }
        }

        private async Task ProcessMessageException(SubscriptionDescription subscription, MessageReceiver receiver, Message message, int cCount)
        {
            // handle service bus message acknowledgement
            if ((cCount + 1) >= subscription.MaxDeliveryCount)
                await receiver.DeadLetterAsync(message.SystemProperties.LockToken, message.UserProperties);
            else
            {
                var newMsg = message.Clone();
                newMsg.UserProperties["RedeliveryCounter"] = cCount + 1;

                await _sender.ScheduleMessage(subscription.TopicPath, newMsg, DateTimeOffset.UtcNow.AddSeconds(Math.Abs(_appSettings.ServiceBus.RedeliveryDelaySeconds)));
                await receiver.CompleteAsync(message.SystemProperties.LockToken);

                _logger.LogInformation("{Delivery} delivery.", "Unsuccessful");
            }
        }

        private DistributorApiClient GetApiClient(ApiConsumer api)
            => api?.IsValid ?? false ?
                new DistributorApiClient(
                    _baseHttpClient.Client,
                    api.Url,
                    new System.Net.Http.Headers.AuthenticationHeaderValue(api.Authentication.Schema, api.Authentication.Parameter)
                ) : default;

        /// <summary>
        /// Raises the exceptions that occurred during batch processing
        /// </summary>
        private void RaiseBatchExceptions(List<WearioSynchronizationBatch> batches, Exception exception = null)
        {
            List<Exception> exceptions = new List<Exception>();

            //add the given exception
            if (exception != null)
            {
                //but check if its an AggregateException in which case we want to add all exceptions
                if (exception is AggregateException aggregateException)
                {
                    exceptions.AddRange(aggregateException.InnerExceptions);
                }
                else
                {
                    exceptions.Add(exception);
                }
            }

            //add the exceptions in the batches as well
            if (batches != null)
            {
                exceptions.AddRange(batches.Where(b => b.Exception != null).Select(b => b.Exception));
            }

            //throw when needed
            if (exceptions.Count > 0)
            {
                throw new AggregateException("An error occurred during the synchronisation process.", exceptions);
            }
        }
    }
}
