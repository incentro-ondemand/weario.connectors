﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Weario.Integration.Application.Helpers
{
    /// <summary>
    /// Helper class for messages
    /// </summary>
    public static class MessageHelper
    {
        /// <summary>
        /// Gets a message fragment for logging purposes
        /// </summary>
        public static string GetLogFragment(string message)
        {
            return message?.Substring(0, Math.Min(message.Length, 250));
        }

        public static string GetLogFragment(object messageObject)
        {
            try
            {
                return GetLogFragment(JsonConvert.SerializeObject(messageObject));
            }
            catch { return ""; }
        }
    }
}
