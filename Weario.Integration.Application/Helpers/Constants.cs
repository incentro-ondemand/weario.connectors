﻿using System;
using System.Collections.Generic;
using System.Text;
using Weario.Integration.Application.Configuration;
using Weario.Integration.Application.Services;

namespace Weario.Integration.Application.Helpers
{
    /// <summary>
    /// Defines the names of the service bus topics
    /// </summary>
    public class Topics
    {
        public const string Orders = "orders";
        public const string FeedbackOrders = "feedbackorders";
        public const string SupplierProducts = "supplierproducts";
        public const string ActiveSupplierProducts = "activesupplierproducts";
        public const string DistributorProducts = "distributorproducts";
        public const string ActiveDistributorProducts = "activedistributorproducts";
    }

    /// <summary>
    /// Defines the names and fieldnames of the config values
    /// </summary>
    public static class TopicSubscriptions
    {
        public static readonly Dictionary<ParticipantGroup, string> ParticipantIDs = new Dictionary<ParticipantGroup, string> {
            { ParticipantGroup.Distributors, "DistributorID" },
            { ParticipantGroup.Suppliers, "SupplierID" }
        };
    }
}
