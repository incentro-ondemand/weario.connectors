﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Weario.Integration.Application.Extensions
{
    /// <summary>
    /// Object extensions
    /// </summary>
    public static class ObjectExtensions
    {
        /// <summary>
        /// Clones a object via shallow copy
        /// </summary>
        public static T CloneObject<T>(this T instance)
        {
            if (instance == null)
            {
                return default(T);
            }

            return (T)instance
                .GetType()
                .GetMethod("MemberwiseClone", BindingFlags.Instance | BindingFlags.NonPublic)
                ?.Invoke(instance, null);
        }
    }
}
