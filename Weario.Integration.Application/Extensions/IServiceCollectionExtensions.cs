﻿using Microsoft.Extensions.DependencyInjection;
using Weario.Integration.Application.Services;

namespace Weario.Integration.Application.Extensions
{
    public static class IServiceCollectionExtensions
    {

        public static IServiceCollection AddWearioApplicationServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<IDeadLetterService, DeadLetterService>();
            serviceCollection.AddSingleton<IEancodeLookupService, EancodeLookupService>();
            serviceCollection.AddSingleton<IMockGenerator, MockGenerator>();
            serviceCollection.AddSingleton<IModelDescriptorFactory, ModelDescriptorFactory>();
            serviceCollection.AddSingleton<IOrderManager, OrderManager>();
            serviceCollection.AddSingleton<IMessageSender, ParallelMessageSender>();
            serviceCollection.AddSingleton<IParticipantService, ParticipantService>();
            serviceCollection.AddSingleton<IServiceBusManager, ServiceBusManager>();
            serviceCollection.AddSingleton<IWearioSynchronizer, WearioSynchronizer>();

            return serviceCollection;
        }

    }
}
