﻿using Microsoft.Extensions.Logging;
using System;

namespace Weario.Integration.Application.Extensions
{
    /// <summary>
    /// Extensions for the ILogger interface
    /// </summary>
    public static class ILoggerExtensions
    {
        /// <summary>
        /// Logs an exception
        /// </summary>
        public static void LogError(this ILogger logger, Exception exception, int? number = null, int? count = null)
        {
            if (exception is AggregateException aggregateException)
            {
                int index = 0;

                logger.LogError(aggregateException.Message);

                foreach (var innerException in aggregateException.InnerExceptions)
                {
                    LogError(logger, innerException, ++index, aggregateException.InnerExceptions.Count);
                }

                return;
            }

            string message = number != null && count != null
                ? String.Format("Exception {0}/{1} was raised: {{exception}}", number, count)
                : "Exception was raised: {exception}";

            logger.LogError(default(EventId), exception, message, exception);
        }
    }
}
