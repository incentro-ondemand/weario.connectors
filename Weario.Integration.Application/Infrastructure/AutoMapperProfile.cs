﻿using AutoMapper;
using System.Collections.Generic;

namespace Weario.Integration.Application.Infrastructure
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            // Distributor Activeproducts Mappings
            CreateMap<DomainModels.PostActiveDistributorProducts, Integration.Infrastructure.Clients.Weario.PostActiveDistributorProductsRequest>().ReverseMap().ForMember(m => m.JobIndentifier, o => o.Ignore());
            CreateMap<DomainModels.ActiveDistributorProduct, Integration.Infrastructure.Clients.Weario.Products4>().ReverseMap();

            // Supplier Activeproducts Mappings
            CreateMap<DomainModels.PostActiveSupplierProducts, Integration.Infrastructure.Clients.Weario.PostActiveSupplierProductsRequest>().ReverseMap().ForMember(m => m.JobIndentifier, o => o.Ignore());
            CreateMap<DomainModels.ActiveSupplierProduct, Integration.Infrastructure.Clients.Weario.Products3>().ReverseMap();

            // Distributor Products Mappings
            CreateMap<DomainModels.PostDistributorProducts, Integration.Infrastructure.Clients.Weario.PostDistributorProductsRequest>().ReverseMap().ForMember(m => m.JobIndentifier, o => o.Ignore());
            CreateMap<DomainModels.DistributorProduct, Integration.Infrastructure.Clients.Weario.Products2>().ReverseMap();
            CreateMap<DomainModels.DistributorColor, Integration.Infrastructure.Clients.Weario.Colors2>().ReverseMap();
            CreateMap<DomainModels.Service, Integration.Infrastructure.Clients.Weario.Services>().ReverseMap();
            CreateMap<DomainModels.RelatedProduct, Integration.Infrastructure.Clients.Weario.RelatedProducts>().ReverseMap();
            CreateMap<DomainModels.DistributorVariant, Integration.Infrastructure.Clients.Weario.ProductVariants>().ReverseMap();
            CreateMap<DomainModels.DistributorSize, Integration.Infrastructure.Clients.Weario.Sizes2>().ReverseMap();
            CreateMap<DomainModels.PriceRange, Integration.Infrastructure.Clients.Weario.PriceRanges>().ReverseMap();
            CreateMap<DomainModels.PriceRange, Integration.Infrastructure.Clients.Weario.PriceRanges2>().ReverseMap();

            // Supplier Products Mappings
            CreateMap<DomainModels.PostSupplierProducts, Integration.Infrastructure.Clients.Weario.PostSupplierProductsRequest>().ReverseMap().ForMember(m => m.JobIndentifier, o => o.Ignore());
            CreateMap<DomainModels.SupplierProduct, Integration.Infrastructure.Clients.Weario.Products>().ReverseMap();
            CreateMap<DomainModels.SupplierLanguageSpecificData, Integration.Infrastructure.Clients.Weario.LanguageSpecificData>().ReverseMap();
            CreateMap<DomainModels.SupplierColor, Integration.Infrastructure.Clients.Weario.Colors>().ReverseMap();
            CreateMap<DomainModels.Image, Integration.Infrastructure.Clients.Weario.DefaultImages>().ReverseMap();
            CreateMap<DomainModels.Branche, Integration.Infrastructure.Clients.Weario.Branches>().ReverseMap();
            CreateMap<DomainModels.Image, Integration.Infrastructure.Clients.Weario.ColorImages>().ReverseMap();
            CreateMap<DomainModels.SupplierSize, Integration.Infrastructure.Clients.Weario.Sizes>().ReverseMap();
            CreateMap<DomainModels.ProductNorm, Integration.Infrastructure.Clients.Weario.Norms>().ReverseMap();

            // Distributor Order Mappings
            CreateMap<DomainModels.Order, Integration.Infrastructure.Clients.Distributor.Order>().ReverseMap();
            CreateMap<DomainModels.Event, Integration.Infrastructure.Clients.Distributor.Event>().ReverseMap();
            CreateMap<DomainModels.Company, Integration.Infrastructure.Clients.Distributor.Company>().ReverseMap();
            CreateMap<DomainModels.Contactperson, Integration.Infrastructure.Clients.Distributor.Contactperson>().ReverseMap();
            CreateMap<DomainModels.Contactinformation, Integration.Infrastructure.Clients.Distributor.Contactinformation>().ReverseMap();
            CreateMap<DomainModels.Number, Integration.Infrastructure.Clients.Distributor.Number>().ReverseMap();
            CreateMap<DomainModels.Adress, Integration.Infrastructure.Clients.Distributor.Adress>().ReverseMap();
            CreateMap<DomainModels.Orderline, Integration.Infrastructure.Clients.Distributor.Orderline>().ReverseMap();
            CreateMap<DomainModels.Product, Integration.Infrastructure.Clients.Distributor.Product>().ReverseMap();
            CreateMap<DomainModels.Attributes, Integration.Infrastructure.Clients.Distributor.Attributes>().ReverseMap();
            CreateMap<DomainModels.OrderlineService, Integration.Infrastructure.Clients.Distributor.OrderlineService>().ReverseMap();
            CreateMap<DomainModels.Timeline, Integration.Infrastructure.Clients.Distributor.Timeline>().ReverseMap();

            // Distributor Order Mappings
            CreateMap<DomainModels.WearioOrders, Integration.Infrastructure.Clients.Weario.PutOrdersRequest>().ReverseMap()
                .ForMember(m => m.DistributorID, o => o.Ignore())
                .ForMember(m => m.JobIndentifier, o => o.Ignore());
            CreateMap<DomainModels.WearioOrder, Integration.Infrastructure.Clients.Weario.Orders>()
                .ForMember(m => m.DistributorOrderID, o => o.MapFrom(s => s.WearioOrderID))
                .ReverseMap();
            CreateMap<DomainModels.WearioOrderLine, Integration.Infrastructure.Clients.Weario.OrderLines>().ReverseMap();
        }
    }
}
