﻿namespace Weario.Integration.Application.Configuration
{
    /// <summary>
    /// Interface that describes a participant group (distributor/supplier)
    /// </summary>
    public enum ParticipantGroup
    {
        Distributors,
        Suppliers,
    }
}

