﻿namespace Weario.Integration.Application.Configuration
{
    public class Distributor : IParticipant
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public ApiConsumer OrderApi { get; set; }
        public ApiConsumer ProductApi { get; set; }
        public ParticipantGroup Group => ParticipantGroup.Distributors;
    }
}

