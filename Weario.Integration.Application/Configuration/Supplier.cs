﻿namespace Weario.Integration.Application.Configuration
{
    public class Supplier: IParticipant
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public ApiConsumer ProductApi { get; set; }
        public ParticipantGroup Group => ParticipantGroup.Suppliers;
    }
}

