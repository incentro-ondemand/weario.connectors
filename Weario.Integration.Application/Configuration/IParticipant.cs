﻿namespace Weario.Integration.Application.Configuration
{
    /// <summary>
    /// Interface that describes a participant 
    /// </summary>
    public interface IParticipant
    {
        string ID { get; }

        string Name { get; }

        string Code { get; }

        ApiConsumer ProductApi { get; }

        ParticipantGroup Group { get; }
    }
}

