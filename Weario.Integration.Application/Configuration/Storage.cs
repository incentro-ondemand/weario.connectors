﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Weario.Integration.Application.Configuration
{
    public class Storage
    {
        public string ConnectionString { get; set; }
        public string TableEancodeLookup { get; set; }
        public string TableSynchronisation { get; set; }
    }
}
