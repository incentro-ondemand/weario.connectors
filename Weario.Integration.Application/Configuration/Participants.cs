﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Weario.Integration.Application.Configuration
{
    public class Participants
    {
        public ICollection<Distributor> Distributors { get; set; }
        public ICollection<Supplier> Suppliers { get; set; }

        public IParticipant Find(string id)
        {
            return (IParticipant)Distributors?.FirstOrDefault(p => string.Equals(p.ID, id, StringComparison.InvariantCultureIgnoreCase)) ??
                   Suppliers?.FirstOrDefault(p => string.Equals(p.ID, id, StringComparison.InvariantCultureIgnoreCase)) ??
                   default;
        }

        public IEnumerable<IParticipant> Members(ParticipantGroup group) =>
            group == ParticipantGroup.Distributors ? Distributors.Cast<IParticipant>() :
            group == ParticipantGroup.Suppliers ? Suppliers.Cast<IParticipant>() :
            throw new Exception($"Unconfigured ParticipantGroup {group}");

        public IParticipant Member(ParticipantGroup group, string id) =>
            this.Members(group).FirstOrDefault(p => p.ID == id) ?? throw new Exception($"ParticipantGroup {group} doesn't contain member with id {id}.");

        // Used for monitoring configuration update. 
        // Read this configuration section when this property is changed.
        public string Sentinel { get; set; }
    }
}

