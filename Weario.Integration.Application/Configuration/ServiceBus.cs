﻿namespace Weario.Integration.Application.Configuration
{
    public class ServiceBus
    {
        public string ConnectionString { get; set; }

        /// <summary>
        /// Gets the maximum number of operations to run in parallel when calling the serviceb us
        /// </summary>
        public int MaxParallelism { get; set; }

        public int RedeliveryDelaySeconds { get; set; }

        public int DefaultMaxDeliveryCount { get; set; }
    }
}

