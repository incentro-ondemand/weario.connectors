﻿namespace Weario.Integration.Application.Configuration
{
    public class WearioApi
    {
        /// <summary>
        /// Gets the Weario API url
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Gets the Weario API username
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets the Weario API password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets the maximum number of operations to run in parallel when calling the weario api
        /// </summary>
        public int MaxParallelism { get; set; }

        /// <summary>
        /// Gets the number of items(products)  send per api request
        /// </summary>
        public int ItemsPerRequest { get; set; }

        /// <summary>
        /// Gets whether we should mock the Weario API. In this case the messages are never synchronized to the Weario API.
        /// </summary>
        public bool Mock { get; set; }
    }
}

