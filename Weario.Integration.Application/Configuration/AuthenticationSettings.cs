﻿namespace Weario.Integration.Application.Configuration
{
    public class AuthenticationSettings
    {
        public string Schema { get; set; }
        public string Parameter { get; set; }
    }

}

