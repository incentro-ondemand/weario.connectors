﻿namespace Weario.Integration.Application.Configuration
{
    public class ApiConsumer
    {
        public bool Enabled { get; set; }
        public string Url { get; set; }
        public AuthenticationSettings Authentication { get; set; }

        public bool IsValid
            => !string.IsNullOrWhiteSpace(Url) && Enabled;
    }
}

