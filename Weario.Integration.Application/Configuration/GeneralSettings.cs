﻿using System;
using System.Linq;
using System.Text;
using Weario.Integration.Application.Helpers;

namespace Weario.Integration.Application.Configuration
{

    public class GeneralSettings
    {
        public ServiceBus ServiceBus { get; set; }

        public WearioApi WearioApi { get; set; }

        public Storage Storage { get; set; }

        /// <summary>
        /// Gets whether to include exception details in API responses.
        /// </summary>
        public bool ExceptionDetails { get; set; }

        /// <summary>
        /// Gets the number of records in a single batch when syncing
        /// </summary>
        public int SyncBatchSize { get; set; }

        /// <summary>
        /// Gets the synchronisation timeout for servicebus retrieval as a timespan
        /// </summary>
        public TimeSpan SyncTimeout { get; set; }

        // Used for monitoring configuration update. 
        // Read this configuration section when this property is changed.
        public string Sentinel { get; set; }
    }
}

