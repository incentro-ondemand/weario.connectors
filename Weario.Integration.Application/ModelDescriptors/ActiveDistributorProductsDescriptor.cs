﻿using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Linq;
using Weario.Integration.Application.Configuration;
using Weario.Integration.Application.DomainModels;
using Weario.Integration.Application.Helpers;

namespace Weario.Integration.Application.ModelDescriptors
{
    /// <summary>
    /// ModelDescriptorBase<T> implementation for describing a PostActiveDistributorProducts model.
    /// </summary>
    public class ActiveDistributorProductsDescriptor : ModelDescriptorBase<PostActiveDistributorProducts>
    {
        public ActiveDistributorProductsDescriptor(IOptions<GeneralSettings> appSettings) : base(appSettings) { }

        public override string ParticipantID
        {
            get => this.Model.DistributorID;
            set => this.Model.DistributorID = value;
        }
        public override string ParticipantKeyField => nameof(this.Model.DistributorID);

        public override IEnumerable<object> Items {
            get => this.Model.Products?.Cast<object>() ?? new object[0];
            set => this.Model.Products = value.Cast<ActiveDistributorProduct>().ToList();
        }

        public override string Topic => Topics.ActiveDistributorProducts;

        public override ParticipantGroup Group => ParticipantGroup.Distributors;

        public override bool SendIndividually => false;

        public override string MessageKeyID => "ProductID";

        public override string MessageKeyValue(PostActiveDistributorProducts modelItem)
            => (modelItem ?? this.Model).Products?.FirstOrDefault().DistributorProductID ?? "";
    }
}
