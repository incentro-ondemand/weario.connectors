﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Reflection;
using Weario.Integration.Application.Services;
using Weario.Integration.Application.Extensions;
using Weario.Integration.Application.Configuration;
using Microsoft.Extensions.Options;

namespace Weario.Integration.Application.ModelDescriptors
{
    /// <summary>
    /// Abstract base class that describes a model instance.
    /// </summary>
    /// <remarks>
    /// We have these ModelDescriptor classes because our models are generated based on Swagger definitions which don't contain nice generic structures.
    /// </remarks>
    public abstract class ModelDescriptorBase<T> where T: IModelDescriptor
    {

        private IOptions<GeneralSettings> _appSettings;

        public ModelDescriptorBase(IOptions<GeneralSettings> appSettings)
        {
            _appSettings = appSettings ?? throw new ArgumentNullException(nameof(appSettings));
        }

        public T Model { get; set; }
        public Type ModelType => typeof(T);

        public abstract string ParticipantID { get; set; }

        public virtual Guid JobIndentifier
        {
            get => this.Model.JobIndentifier;
            set => this.Model.JobIndentifier = value;
        }

        public abstract IEnumerable<object> Items { get; set; }

        public abstract string ParticipantKeyField { get; }

        public abstract string Topic { get; }

        public abstract ParticipantGroup Group { get; }

        public abstract bool SendIndividually { get; }

        public virtual int SyncBatchSize => this.SendIndividually ? _appSettings.Value.SyncBatchSize : 1;

        public IEnumerable<T> CreateModelPerItem()
        {
            //create another descriptor of the same type
            var cloneDescriptor = Activator.CreateInstance(this.GetType(), _appSettings) as ModelDescriptorBase<T>;
            
            //create a clone for each item
            var clones = this.Items.Select(p => {

                var clone = this.Model.CloneObject();

                //we use the ModelDescriptor so we can work with a generic items collection instead of the generated models.
                cloneDescriptor.Model = clone;
                cloneDescriptor.Items = new[] { p };

                return clone;

            }).ToList();

            return clones;
        }

        public abstract string MessageKeyID { get; }

        public virtual string MessageKeyValue(T modelItem) { return ""; }
}
}
