﻿using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Linq;
using Weario.Integration.Application.Configuration;
using Weario.Integration.Application.DomainModels;
using Weario.Integration.Application.Helpers;

namespace Weario.Integration.Application.ModelDescriptors
{
    /// <summary>
    /// ModelDescriptorBase<T> implementation for describing a PostSupplierProducts model.
    /// </summary>
    public class FeedbackOrdersDescriptor : ModelDescriptorBase<WearioOrders>
    {
        public FeedbackOrdersDescriptor(IOptions<GeneralSettings> appSettings) : base(appSettings) { }

        public override string ParticipantID
        {
            get => this.Model.DistributorID;
            set => this.Model.DistributorID = value;
        }

        public override string ParticipantKeyField => nameof(this.Model.DistributorID);

        public override IEnumerable<object> Items
        {
            get => this.Model.Orders?.Cast<object>() ?? new object[0];
            set => this.Model.Orders = value.Cast<WearioOrder>().ToList();
        }

        public override string Topic => Topics.FeedbackOrders;

        public override ParticipantGroup Group => ParticipantGroup.Distributors;

        public override bool SendIndividually => true;

        public override string MessageKeyID => "OrderID";

        public override string MessageKeyValue(WearioOrders modelItem)
            => (modelItem ?? this.Model).Orders?.FirstOrDefault().WearioOrderID ?? "";
    }
}
