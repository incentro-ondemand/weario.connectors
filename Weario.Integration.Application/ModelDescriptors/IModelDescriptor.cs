﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Weario.Integration.Application.ModelDescriptors
{
    public interface IModelDescriptor
    {
        Guid JobIndentifier { get; set; }
    }
}
