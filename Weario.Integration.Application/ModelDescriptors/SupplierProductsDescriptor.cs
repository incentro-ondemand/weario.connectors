﻿using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Linq;
using Weario.Integration.Application.Configuration;
using Weario.Integration.Application.DomainModels;
using Weario.Integration.Application.Helpers;

namespace Weario.Integration.Application.ModelDescriptors
{
    /// <summary>
    /// ModelDescriptorBase<T> implementation for describing a PostSupplierProducts model.
    /// </summary>
    public class SupplierProductsDescriptor : ModelDescriptorBase<PostSupplierProducts>
    {
        public SupplierProductsDescriptor(IOptions<GeneralSettings> appSettings) : base(appSettings) { }

        public override string ParticipantID
        {
            get => this.Model.SupplierID;
            set => this.Model.SupplierID = value;
        }

        public override string ParticipantKeyField => nameof(this.Model.SupplierID);

        public override IEnumerable<object> Items
        {
            get => this.Model.Products?.Cast<object>() ?? new object[0];
            set => this.Model.Products = value.Cast<SupplierProduct>().ToList();
        }

        public override string Topic => Topics.SupplierProducts;

        public override ParticipantGroup Group => ParticipantGroup.Suppliers;

        public override bool SendIndividually => true;

        public override string MessageKeyID => "ProductID";

        public override string MessageKeyValue(PostSupplierProducts modelItem)
            => (modelItem ?? this.Model).Products?.FirstOrDefault().SupplierProductID ?? "";

    }
}
