﻿using System.Collections.Generic;

namespace Weario.Integration.Application.DomainModels
{
    public class DistributorColor
    {
        public string ColorName { get; set; }
        public ICollection<DistributorSize> Sizes { get; set; }
    }
}