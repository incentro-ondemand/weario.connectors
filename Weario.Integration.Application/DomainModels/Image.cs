﻿namespace Weario.Integration.Application.DomainModels
{
    public class Image
    {
        public string ImageURL { get; set; }
        public int? SortOrder { get; set; }
    }
}