﻿using System.Collections.Generic;

namespace Weario.Integration.Application.DomainModels
{
    public class DistributorProduct : IWearioProduct
    {
        public string DistributorProductID { get; set; }
        public long? EANCode { get; set; }
        public string SupplierID { get; set; }
        public string SupplierProductID { get; set; }
        public bool UsesEANCodeIdentification { get; set; }
        public ICollection<DistributorColor> Colors { get; set; }
        public ICollection<Service> Services { get; set; }
        public ICollection<RelatedProduct> RelatedProducts { get; set; }
        public ICollection<DistributorVariant> ProductVariants { get; set; }
    }
}