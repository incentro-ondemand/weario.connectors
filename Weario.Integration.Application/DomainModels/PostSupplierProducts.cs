﻿using System;
using System.Collections.Generic;
using Weario.Integration.Application.ModelDescriptors;

namespace Weario.Integration.Application.DomainModels
{
    public class PostSupplierProducts: IModelDescriptor
    {
        public string SupplierID { get; set; }
        public Guid JobIndentifier { get; set; }
        public ICollection<SupplierProduct> Products { get; set; }
    }
}