﻿namespace Weario.Integration.Application.DomainModels
{
    public class ActiveDistributorProduct
    {
        public string SupplierID { get; set; }
        public string DistributorProductID { get; set; }
    }
}