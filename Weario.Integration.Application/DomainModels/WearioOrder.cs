﻿using System;
using System.Collections.Generic;
using Weario.Integration.Application.ModelDescriptors;

namespace Weario.Integration.Application.DomainModels
{

    public class WearioOrders : IModelDescriptor
    {
        public string DistributorID { get; set; }
        public Guid JobIndentifier { get; set; }
        public ICollection<WearioOrder> Orders { get; set; }
    }

    public class WearioOrder
    {
        public string WearioOrderID { get; set; }
        public double? ConsolidatedPrice { get; set; }
        public WearioOrdersStatus? Status { get; set; }
        public ICollection<WearioOrderLine> OrderLines { get; set; }

        public Order DistributionOrder { get; set; }
        public String FailureReason { get; set; }
    }

    public class WearioOrderLine
    {
        public WearioOrderLinesStatus? Status { get; set; }
        public string WearioOrderLineID { get; set; }
    }
}
