﻿namespace Weario.Integration.Application.DomainModels
{
    public class SupplierSize
    {
        public long? SupplierProductVariantEANCode { get; set; }
        public string SupplierProductVariantID { get; set; }
        public string PrimarySizeValue { get; set; }
        public PrimarySizeType? PrimarySizeType { get; set; }
        public string SecondarySizeValue { get; set; }
        public SecondarySizeType? SecondarySizeType { get; set; }
    }
}