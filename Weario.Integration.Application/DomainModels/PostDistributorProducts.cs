﻿using System;
using System.Collections.Generic;
using Weario.Integration.Application.ModelDescriptors;

namespace Weario.Integration.Application.DomainModels
{
    public class PostDistributorProducts : IModelDescriptor
    {
        public string DistributorID { get; set; }
        public Guid JobIndentifier { get; set; }
        public ICollection<DistributorProduct> Products { get; set; }
    }
}