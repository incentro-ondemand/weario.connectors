﻿using System;
using System.Collections.Generic;
using Weario.Integration.Application.ModelDescriptors;

namespace Weario.Integration.Application.DomainModels
{
    public class PostActiveSupplierProducts : IModelDescriptor
    {
        public string SupplierID { get; set; }
        public Guid JobIndentifier { get; set; }
        public ICollection<ActiveSupplierProduct> Products { get; set; }
    }
}