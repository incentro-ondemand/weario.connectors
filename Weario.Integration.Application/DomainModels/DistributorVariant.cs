﻿using System.Collections.Generic;

namespace Weario.Integration.Application.DomainModels
{
    public class DistributorVariant
    {
        public long? DistributorProductVariantEANCode { get; set; }
        public string DistributorProductVariantID { get; set; }
        public int? DeliveryWorkingDays { get; set; }
        public ICollection<PriceRange> PriceRanges { get; set; }
    }
}
