﻿using Microsoft.Azure.Cosmos.Table;
using System;
using System.Collections.Generic;
using System.Text;
using Weario.Integration.Application.Configuration;

namespace Weario.Integration.Application.DomainModels
{
    public class SynchronisationEntity : TableEntity
    {
        public SynchronisationEntity() { }
        public SynchronisationEntity(ParticipantGroup participantGroup, Guid participantID)
        {
            this.PartitionKey = participantGroup.ToString();
            this.RowKey = participantID.ToString();
        }

        public ParticipantGroup ParticipandType
        {
            get
            {
                if (Enum.TryParse<ParticipantGroup>(this.PartitionKey, out var t)) return t;
                return default;
            }
        }
        public Guid ParticipandID
        {
            get
            {
                if (Guid.TryParse(this.RowKey, out var t)) return t;
                return Guid.Empty;
            }
        }
        public Guid? JobID { get; set; }
        public DateTime? StartDate { get; set; }
    }
}
