﻿using System.Collections.Generic;

namespace Weario.Integration.Application.DomainModels
{
    public class SupplierColor
    {
        public ColorCategory? ColorCategory { get; set; }
        public string ColorName { get; set; }
        public ICollection<Image> ColorImages { get; set; }
        public ICollection<SupplierSize> Sizes { get; set; }
        public string ColorHexCode { get; set; }
    }
}