﻿using Microsoft.Azure.Cosmos.Table;

namespace Weario.Integration.Application.DomainModels
{
    public class EancodeLookupEntity : TableEntity
    {
        public EancodeLookupEntity() { }
        public EancodeLookupEntity(string eancode, string supplierID)
        {
            this.RowKey = eancode;
            this.PartitionKey = supplierID;
        }

        public string Eancode => this.RowKey;
        public string SupplierID => this.PartitionKey;
        public string SupplierProductID { get; set; }
        public string SupplierProductVariantID { get; set; }

    }
}
