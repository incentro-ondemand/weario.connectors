﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Weario.Integration.Application.DomainModels
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum Norm
    {
        EN_342,
        EN_343,
        EN_470,
        EN_470_1,
        EN_531,
        EN_1149,
        EN_11611,
        EN_13034,
        EN_13034_6,
        EN_14058,
        EN_IEC_61482,
        EN_ISO_11612,
        EN_ISO_14116,
        EN_ISO_20345,
        EN_ISO_20347,
        EN_ISO_20471,
        ISO_11611,
        ISO_11612,
        ISO_14116,
        ISO_20471,
        NONE,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ProductConstruction
    {
        AMERICANFLEECE,
        BIRDSEYE,
        CANVAS,
        DENIM,
        DOBBYPOLYESTER,
        INTERLOCKFLEECE,
        MESH,
        MICROFLEECE,
        OXFORD,
        OXFORDCARBONFIBRETHREAD,
        PINPOINTOXFORD,
        PIQUE,
        PLAINWEAVE,
        POLARFLEECE,
        POLYESTERMECHANICALSTRETCH,
        POPLIN,
        SINGLEJERSEY,
        TWILL,
        TWILLMECHANICALSTRETCH,
        WAROKNIT,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ProductCut
    {
        PLUSSIZE,
        REGULAR,
        SLIMFIT,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ProductMaterial
    {
        BAMBOO,
        COOLDRY,
        CORDURA,
        COTTON,
        COTTONELASTANE,
        COTTONPOLYESTER,
        DENIM,
        ELASTANE,
        FLEECE,
        FULLGRAINLEATHER,
        LEATHER,
        MIX,
        NYLON,
        POLYESTER,
        POLYESTERELASTANE,
        POLYESTERSPANDEX,
        TOPGRAINLEATHER,
        WOOL,
        WOOLPOLYESTER,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ProductMaterialMidSole
    {
        COMPOSITE,
        WOVEN,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ProductNeck
    {
        CREWNECK,
        DEEPNECK,
        HOODIE,
        LOWVNECK,
        POLO,
        VNECK,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ProductSex
    {
        CHILDREN,
        FEMALE,
        MALE,
        UNISEX,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ProductMaterialSole
    {
        EVA,
        EVA_RUBBER,
        PU,
        PU_PU,
        PU_RUBBER,
        PU_TPU,
        RUBBER,
        TPU,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ProductSleeveType
    {
        LONGSLEEVE,
        NOSLEEVE,
        SHORTSLEEVE,
        THREEQUARTERSLEEVE,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ProductCategory
    {
        _3MFILTERS,
        AMERICANOVERALLS,
        ANCHORPOINTS,
        ASBESTOSOVERALLS,
        BANDEDEARPLUGS,
        BASEBALLCAPS,
        BELTS,
        BLAZERS,
        BLOWERSYSTEMS,
        BODYWARMERS,
        BOOTS,
        CARDIGANS,
        CHEMICALRESISTANTGLOVES,
        CHINOS,
        CLEANINGCLOTHS,
        CLOGS,
        COATS,
        COMBIFILTERS,
        CONSTRUCTIONGLOVES,
        CORDS,
        COSTGUARDPOLOS,
        CUTRESISTANTGLOVES,
        CYCLINGSHORTS,
        DRESSES,
        DUSTFILTERS,
        DUSTMASKS,
        EARMUFFS,
        EARPLUGS,
        EARPLUGSDISPENCERS,
        EYESHOWERS,
        EYEWASHBOTTLES,
        EYEWASHSTATION,
        FACESHIELDS,
        FALLARRESTBLOCKS,
        FALLPROTECTIONCLAMPS,
        FILTERCARTRIDGEHOLDERS,
        FINEDUSTFILTERS,
        FINEDUSTMASKS,
        FIRSTAIDKITS,
        FIRSTAIDKITSLARGE,
        FIRSTAIDKITSREFILL,
        FIRSTAIDKITSSMALL,
        FLAMERETARDANDCOATS,
        FLAMERETARDANDOVERALLS,
        FLAMERETARDANDTHERMALSHIRTS,
        FLAMERETARDANDWORKPANTS,
        FOLDINGMASKS,
        FULLFACEMASKS,
        GASFILTERS,
        GLOVES,
        GOGGLES,
        GRIPGLOVES,
        HAIRNETCAPS,
        HALFFACEMASKS,
        HARNESSES,
        HATS,
        HEADBANDS,
        HELMETS,
        HOODYS,
        HOSPITALSHIRTS,
        HYGIENEKITS,
        INSOLES,
        JACKETS,
        JEANS,
        KNEEPADS,
        LIGHTOVERALLS,
        MASKS,
        NECKPROTECTORS,
        PANTS,
        PARKAS,
        PILOTCOATS,
        PILOTJACKETS,
        PLUGINFIXATION,
        POLOSHIRTS,
        POLOSWEATERS,
        POSITIONING,
        PULLYS,
        QUILTEDCOATS,
        RAINCOATS,
        RAINOVERALLS,
        RAINPANTS,
        ROPES,
        SAFETYBODYPANTS,
        SAFETYCAPS,
        SAFETYCOATS,
        SAFETYGLOVES,
        SAFETYGOGGLES,
        SAFETYHATS,
        SAFETYHELMETS,
        SAFETYLINES,
        SAFETYOVERALLS,
        SAFETYPANTS,
        SAFETYPOLOS,
        SAFETYSCREENS,
        SAFETYSHOES,
        SAFETYSWEATERS,
        SAFETYVESTS,
        SCARFS,
        SCREWFILTERS,
        SHIRTS,
        SHOEMAINTENANCE,
        SNAPHOOKS,
        SOCKS,
        SOFTSHELLS,
        SWEATERS,
        SWEATVESTS,
        SWIMMINGTRUNKS,
        THERMALGLOVES,
        THERMALPANTS,
        THERMALSHIRTS,
        THERMALSWEATERS,
        TIES,
        TRACKSUITPANTS,
        TRAFFICCONTROLERCOATS,
        TSHIRTS,
        VIOLENCECONTROL,
        WAISTCOATS,
        WELDINGGLOVES,
        WELDINGHOODS,
        WINTERCOATS,
        WINTERGLOVES,
        WORKINGSHOES,
        WORKPANTS,
        WORKSHIRTS,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ProductClosure
    {
        BUTTONS,
        ELASTIC,
        LACES,
        VELCRO,
        ZIPPER,
        NONE,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ProductCollection
    {
        CASUAL,
        CORPORATEFASHION,
        WORKWEAR,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ProductHoodieDetails
    {
        CORD,
        DETACHABLE,
        NONE,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ProductLining
    {
        DOWN,
        NONE,
        OTHER,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ProductMaterialPBM
    {
        COWGRAINLEATHER,
        COWSPLITLEATHER,
        KNITTED,
        LATEX,
        LEATHERLINING,
        NEOPRENE,
        NITRILE,
        NITRILECOATING,
        PUCOATING,
        SHEEPGRAINLEATHER,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ProductPocketsType
    {
        BUTTON,
        NONE,
        OPEN,
        ZIPPER,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ProductShoeModel
    {
        HIGH,
        LOW,
        REGULAR,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ProductShoeNorm
    {
        O2,
        O3,
        S1,
        S1P,
        S2,
        S3,
        S4,
        S5,
        S6,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ProductSafetyNose
    {
        ALLUMINIUM,
        COMPOSITE,
        NONE,
        STEEL,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum YesNoBool
    {
        NO,
        YES,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ColorCategory
    {
        BLACK,
        BLUE,
        BROWN,
        GREEN,
        GREY,
        NOTAPPLICABLE,
        ORANGE,
        PINK,
        PURPLE,
        RED,
        TRANSPARANT,
        WHITE,
        YELLOW,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum BrancheName
    {
        AGRICULTURE,
        CATERINGHOTELS,
        CONSTRUCTION,
        ENVIRONMENTALMANAGEMENT,
        HEALTHCAREHYGIENE,
        HOSPITALITY,
        INDUSTRYCHEMISTRYOFFSHORE,
        INSTALLATIONSERVICES,
        LOGISTICS,
        PAINTERS,
        RETAIL,
        SECURITY,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum LanguageCode
    {
        NL,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ServicesName
    {
        PRINT,
        EMBROIDER,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum PrimarySizeType
    {
        CONFECTIONFEMALE_NL,
        CONFECTIONMALE_NL,
        GLOVESIZE_NL,
        NECKSIZE_CM,
        ONESIZE_BOOLEAN,
        SHOESIZE_NL,
        SIZERANGE_STRING,
        UNISEX_ENUM,
        WIDTH_INCHES,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SecondarySizeType
    {
        LENGTH_INCHES,
        SLEEVELENGTH_INTEGER,
        CONFECTIONSIZECATEGORY_ENUM,
        CLASSIFICATION_ENUM,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum WearioOrdersStatus
    {
        UPDATEDBYDATACON = 0,
        FINALIZEDBYDATACON = 1,
        FAILED = 2,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum WearioOrderLinesStatus
    {
        CANCELLEDBYDISTRIBUTOR = 0,
        SHIPPEDBYDISTRIBUTOR = 1,
    }
}