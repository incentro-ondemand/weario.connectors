﻿using System.Collections.Generic;

namespace Weario.Integration.Application.DomainModels
{
    public class SupplierProduct : IWearioProduct
    {
        public string Brand { get; set; }
        public ProductConstruction? Construction { get; set; }
        public ProductCut? Cut { get; set; }
        public long? EANCode { get; set; }
        public ProductMaterial? Material { get; set; }
        public ProductMaterialMidSole? MaterialMidSole { get; set; }
        public ProductNeck? Neck { get; set; }
        public ICollection<ProductNorm> Norms { get; set; }
        public ProductSex? Sex { get; set; }
        public ProductMaterialSole? MaterialSole { get; set; }
        public ProductSleeveType? SleeveType { get; set; }
        public ProductCategory? ProductCategory { get; set; }
        public ICollection<SupplierLanguageSpecificData> LanguageSpecificData { get; set; }
        public ICollection<SupplierColor> Colors { get; set; }
        public ICollection<Image> DefaultImages { get; set; }
        public ICollection<Branche> Branches { get; set; }
        public ProductClosure? Closure { get; set; }
        public ProductCollection? Collection { get; set; }
        public ProductHoodieDetails? HoodieDetails { get; set; }
        public ProductLining? Lining { get; set; }
        public ProductMaterialPBM? MaterialPBM { get; set; }
        public ProductPocketsType? PocketsType { get; set; }
        public string SupplierProductID { get; set; }
        public ProductShoeModel? ShoeModel { get; set; }
        public ProductShoeNorm? ShoeNorm { get; set; }
        public ProductSafetyNose? SafetyNose { get; set; }
        public YesNoBool? ChemicalResistant { get; set; }
        public YesNoBool? Durable { get; set; }
        public YesNoBool? ElectroInsulated { get; set; }
        public YesNoBool? FireResistant { get; set; }
        public YesNoBool? Reflective { get; set; }
        public YesNoBool? ThermoInsulated { get; set; }
        public YesNoBool? WaterRepellent { get; set; }
        public YesNoBool? BranchSpecific { get; set; }
        public YesNoBool? DryCleaningAllowed { get; set; }
    }
}