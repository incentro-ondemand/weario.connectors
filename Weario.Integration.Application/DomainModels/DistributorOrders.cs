﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NJsonSchema.Annotations;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Weario.Integration.Application.ModelDescriptors;

namespace Weario.Integration.Application.DomainModels
{
    public class DistributorOrders : IModelDescriptor
    {
        public string DistributorID { get; set; }
        public Guid JobIndentifier { get; set; }
        public ICollection<Order> Orders { get; set; }
    }

    public class Order
    {
        public string WearioOrderId { get; set; }
        public string CustomerOrderRemarks { get; set; }
        public string CustomerOrderReference { get; set; }
        public Event[] Events { get; set; }
        public Company Company { get; set; }
        public Orderline[] OrderLines { get; set; }
        public string Logo { get; set; }
    }

    public class Company
    {
        public string Name { get; set; }
        public Number[] Numbers { get; set; }
        public Contactperson[] ContactPersons { get; set; }
        public Contactinformation[] ContactInformation { get; set; }
        public Adress[] Adresses { get; set; }
    }

    public class Number
    {
        public string Type { get; set; }
        public string Country { get; set; }
        public string Value { get; set; }
    }

    public class Contactperson
    {
        public string Name { get; set; }
        public Contactinformation[] ContactInformation { get; set; }
    }

    public class Contactinformation
    {
        public string Type { get; set; }
        public string Value { get; set; }
    }

    public class Adress
    {
        public string Type { get; set; }
        public string Streetname { get; set; }
        public string Housenumber { get; set; }
        public string HousenumberSuffix { get; set; }
        public string City { get; set; }
        public string Postcode { get; set; }
        public string CountryCode { get; set; }
    }

    public class Event
    {
        public DateTime Date { get; set; }
        public string Type { get; set; }
        public string Remark { get; set; }
    }

    public class Orderline
    {
        public string WearioOrderLineID { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public Event[] Events { get; set; }
        public OrderlineService[] Services { get; set; }
        public Timeline[] Timeline { get; set; }
    }

    public class Product
    {
        public string WearioId { get; set; }
        public string DistributorProductId { get; set; }
        public string DistributorProductVariantId { get; set; }
        public string Ean { get; set; }
        public float Price { get; set; }
        public string CurrencyCode { get; set; }
        public Attributes Attributes { get; set; }
    }

    public class Attributes
    {
        public string PrimarySizeValue { get; set; }
        public string PrimarySizeType { get; set; }
        public string SecondarySizeValue { get; set; }
        public string SecondarySizeType { get; set; }
        public string Colour { get; set; }
    }

    public class OrderlineService
    {
        public string Description { get; set; }
        public string Type { get; set; }
        public string CurrencyCode { get; set; }
        public float PriceIndication { get; set; }
        public int NumberOfColors { get; set; }
        public ImageSize ImageSize { get; set; }
        public string ImagePosition { get; set; }
    }

    public class Timeline
    {
        public DateTime Date { get; set; }
        public string Type { get; set; }
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum ImageSize
    {
        [EnumMember(Value = "S")]
        SMALL,
        [EnumMember(Value = "M")]
        MEDIUM,
        [EnumMember(Value = "L")]
        LARGE,
    }
}
