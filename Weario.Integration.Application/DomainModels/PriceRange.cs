﻿namespace Weario.Integration.Application.DomainModels
{
    public class PriceRange
    {
        public int? RangeFrom { get; set; }
        public int? RangeTo { get; set; }
        public double? UnitPrice { get; set; }
    }
}