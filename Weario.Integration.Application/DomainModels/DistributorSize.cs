﻿using System.Collections.Generic;

namespace Weario.Integration.Application.DomainModels
{
    public class DistributorSize
    {
        public long? DistributorProductVariantEANCode { get; set; }
        public string DistributorProductVariantID { get; set; }
        public int? DeliveryWorkingDays { get; set; }
        public string PrimarySizeValue { get; set; }
        public PrimarySizeType? PrimarySizeType { get; set; }
        public string SecondarySizeValue { get; set; }
        public SecondarySizeType? SecondarySizeType { get; set; }
        public ICollection<PriceRange> PriceRanges { get; set; }
    }
}