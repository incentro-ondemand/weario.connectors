﻿namespace Weario.Integration.Application.DomainModels
{
    public class SupplierLanguageSpecificData
    {
        public LanguageCode? LanguageCode { get; set; }
        public string ProductDescription { get; set; }
        public string ProductName { get; set; }
    }
}