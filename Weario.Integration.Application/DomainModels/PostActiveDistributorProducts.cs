﻿using System;
using System.Collections.Generic;
using Weario.Integration.Application.ModelDescriptors;

namespace Weario.Integration.Application.DomainModels
{
    public class PostActiveDistributorProducts : IModelDescriptor
    {
        public string DistributorID { get; set; }
        public Guid JobIndentifier { get; set; }
        public ICollection<ActiveDistributorProduct> Products { get; set; }
    }
}